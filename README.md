# README

Используемое окружение:

* Ruby, версия 2.2.3

* Rails, версия 5.0.1

* Клонирование репозитория по SSH

```
#!bash

git clone git@bitbucket.org:esdp-2017-3/klassiki.git
```

### Установка гемов ###

```
#!bash

bundle install
```

### PostgreSQL ###

Приложение использует систему управления базами данных PostgreSQL.

Установка и запуск:

```
#!bash

sudo apt-get install postgresql postgresql-contrib libpq-dev

sudo -u postgres createuser -s `whoami`
```

### Mailer ###

Перед использованием рассылки (**в том числе для выполнения приемочных тестов**) для подтверждения регистрации, необходимо выполнить  в терминале комманду


```
#!bash

mailcatcher
```

### Работа с Git ###

```
#!bash

git add .
git commit -m "#НомерТикета Комментарий к коммиту"
git pull --rebase Выполнять только через RubyMine командой Control+T или Command+T
git push
```