class AddTournamentImageToTournaments < ActiveRecord::Migration[5.0]
  def change
    add_column :tournaments, :tournament_image, :string
  end
end
