class AddActiveToTournaments < ActiveRecord::Migration[5.0]
  def change
    add_column :tournaments, :active, :boolean, default: false
  end
end
