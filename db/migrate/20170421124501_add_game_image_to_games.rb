class AddGameImageToGames < ActiveRecord::Migration[5.0]
  def change
    add_column :games, :game_image, :string
  end
end
