class RenameTournamentStartToStartTime < ActiveRecord::Migration[5.0]
  def change
    rename_column :games, :tournament_start, :start_time
  end
end
