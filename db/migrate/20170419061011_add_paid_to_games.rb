class AddPaidToGames < ActiveRecord::Migration[5.0]
  def change
    add_column :games, :paid, :boolean, default: false
  end
end
