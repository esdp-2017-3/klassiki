class AddAboutToQuestions < ActiveRecord::Migration[5.0]
  def change
    add_column :questions, :about, :string
  end
end
