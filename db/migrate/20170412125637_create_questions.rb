class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.text :body
      t.text :images, array: true
      t.text :audios, array: true
      t.string :answer_desc
      t.string :format
      t.integer :score
      t.integer :stage
      t.references :game, foreign_key: true

      t.timestamps
    end
  end
end
