class AddBanTimeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :ban_time, :datetime
  end
end
