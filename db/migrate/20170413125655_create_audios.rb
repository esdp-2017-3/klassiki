class CreateAudios < ActiveRecord::Migration[5.0]
  def change
    create_table :audios do |t|
      t.string :attachment
      t.references :question, foreign_key: true

      t.timestamps
    end
  end
end
