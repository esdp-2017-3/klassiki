class RemoveImagesFromQuestions < ActiveRecord::Migration[5.0]
  def change
    remove_column :questions, :images, :text
  end
end
