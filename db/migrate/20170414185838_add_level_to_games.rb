class AddLevelToGames < ActiveRecord::Migration[5.0]
  def change
    add_reference :games, :level, index: true
  end
end
