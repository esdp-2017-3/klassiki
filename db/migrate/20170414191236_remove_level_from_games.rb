class RemoveLevelFromGames < ActiveRecord::Migration[5.0]
  def change
    remove_column :games, :level, :string
  end
end
