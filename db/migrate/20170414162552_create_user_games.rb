class CreateUserGames < ActiveRecord::Migration[5.0]
  def change
    create_table :user_games do |t|
      t.references :user, foreign_key: true
      t.references :game, foreign_key: true
      t.datetime :stage1_start_time
      t.datetime :stage2_start_time
      t.datetime :stage1_finish_time
      t.datetime :stage2_finish_time
      t.integer :total_score
      t.float :total_time

      t.timestamps
    end
  end
end
