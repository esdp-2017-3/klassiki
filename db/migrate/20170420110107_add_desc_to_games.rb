class AddDescToGames < ActiveRecord::Migration[5.0]
  def change
    add_column :games, :desc, :text
  end
end
