class RemoveAudiosFromQuestions < ActiveRecord::Migration[5.0]
  def change
    remove_column :questions, :audios, :text
  end
end
