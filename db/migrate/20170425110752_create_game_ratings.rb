class CreateGameRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :game_ratings do |t|
      t.references :user, index: true
      t.references :game, index: true
      t.integer :rating

      t.timestamps
    end
  end
end
