class AddGamesCountToTournaments < ActiveRecord::Migration[5.0]
  def change
    add_column :tournaments, :games_qty, :integer
  end
end
