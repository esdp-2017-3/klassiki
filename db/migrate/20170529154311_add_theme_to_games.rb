class AddThemeToGames < ActiveRecord::Migration[5.0]
  def change
    add_reference :games, :theme, foreign_key: true
  end
end
