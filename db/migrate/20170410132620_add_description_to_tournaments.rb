class AddDescriptionToTournaments < ActiveRecord::Migration[5.0]
  def change
    add_column :tournaments, :description, :text
  end
end
