# Путь для картинок
dir = Rails.root + 'app' + 'assets' + 'images'
dir2 = Rails.root + 'app' + 'assets' + 'images' + 'game_images'
dir3 =  Rails.root + 'app' + 'assets' + 'images' + 'postimages'
# Создание пользователей
admin = User.create!(email: 'admin@example.com', password: 'password', role: 'admin', alias: 'Admin', confirmed_at: DateTime.now, avatar: File.new(dir + "admin.jpg"))
admin.save
user = User.create!(email: 'user@example.com', password: 'password', role: 'none', alias: 'User', confirmed_at: DateTime.now, avatar: File.new(dir + "user.jpg"))
user.save

# Создание уровней
Level.create!(name: 'Beginner',
              description: 'Beginner - игры, сложность которых является минимальной. Задания в играх рассчитаны на игроков,которые только знакомятся с особенностью вопросов игры.')
Level.create!(name: 'New Wave',
              description: 'New Wave level - игры средней сложности, предназначенные для игроков, которые уже поняли принцип построения и отгадывания вопросов, но игровой опыт еще не столь велик, чтобы перейти к играм сложных тем и вопросов.')
Level.create!(name: 'Old School',
              description: 'Old School level - в основу игр данного уровня легли серьезные исторические, культурные, политические темы, призывающие игроков воспользоваться не только логическим мышлением, но и применить свои знания по теме игры. Задания рассчитаны на игроков, которые уже сыграли ни одну игру данного проекта и готовы к нестандартным постановкам вопросов и к долгим поискам ответов.')
Level.create!([{name: 'Детские'},
               {name: 'Корпоративные'}])

# Создание тематик
themes = Theme.create!([{name: 'Литература'}, {name: 'География'}, {name: 'История'}, {name: 'Страны'}, {name: 'Алфавит'},
                        {name: 'Планеты'}, {name: 'Красота'}, {name: 'Жизнь'}, {name: 'Деревья'}])

# Игра книжные герои 1-ый тур
presentation_game = Game.create(game_image: File.new(dir2 + "book_st.jpg"), title: 'Книжные Герои', desc: 'Здесь будет описание', level_id: 1, paid: false, active: true, theme: themes.sample)
question_1 = Question.create!(about: 'Вопрос про прыгательные аттракционы', body: 'В основе названия некого прыгательного аттракциона лежит имя именно этого героя', format: 'Имя',answer_desc: 'Прыгалка тарзанка - в ее названии явно есть имя Тарзан', score: 1, stage: 1, game: presentation_game)
Answer.create!(body: 'Тарзан', question: question_1)
question_2 = Question.create!(about: 'Вопрос про сказачного персонажа',body: 'Этот сказочный персонаж является по сути тем же, с чего начинается и пушкинское побережье, и сахарное восточное лакомство. Как зовут этого персонажа?', format: 'Имя', answer_desc: 'И Лукоморье, и лукум - начинаются с одного и того же слова - лук, а имя сказочного персонажа - луковички - Чиполлино', score: 2, stage: 1, game: presentation_game)
Answer.create!(body: 'Чиполлино', question: question_2)
question_3 = Question.create!(about: 'Вопрос про металл',body: '15 чел/1 см + 1 б.р. Назовите металл, из которого состоит одна из фамилий отрицательного персонажа в произведении, откуда заимствована загаданная фраза', format: 'Название металла', answer_desc: '15 человек/1 Сундук Мертвеца + 1 бутылка рома. Это фраза из песни в романе «Остров Сокровищ», где одним из отрицательных персонажей был пират Джон Сильвер (Silver в переводе означает серебро)', score: 3, stage: 1, game: presentation_game)
Answer.create!(body: 'Серебро', question: question_3)
question_4 = Question.create!(about: 'Вопрос про иллюзиониста',body: 'Пендель советско-российского иллюзиониста', format: 'Имя',answer_desc: 'Пендель= пинок, иллюзионист – Игорь Кио', score: 1, stage: 1, game: presentation_game)
Answer.create!(body: 'Пиноккио', question: question_4)
question_5 = Question.create!(about: 'Вопрос про девочку',body: 'Весь свой путь в некоей сказке эта девочка шла к своему «Хорошему выигрышу» Как звали эту девочку?', format: 'Имя',answer_desc: 'Хороший выигрыш в переводе это Good Win. То есть волшебник Гудвин из сказки «Волшебник Изумрудного города»', score: 2, stage: 1, game: presentation_game)
Answer.create!(body: 'Элли', question: question_5)
question_6 = Question.create!(about: 'Вопрос про растения',body: 'Новогодний персонаж без молибденовых растений Вторая часть в прозвище одного из «Джентльменов Удачи» Безбилетник Опытный моряк Конфетный персонаж, находящийся на антиюге. Назовите двух героев сказки, которые остались не загаданными в задании.', format: 'Имя и Имя',answer_desc: 'Дед Мороз (мо- обозначение молибена, растения – розы) – остается Дед Один из «Джентльменов Удачи» - Али-Баба- вторая часть прозвища - Баба Безбилетник – пассажир без билета-заяц Опытный моряк – Волк Мишка из конфет «Мишка на Севере»', score: 2, stage: 1, game: presentation_game)
Answer.create!(body: 'Лиса и Колобок', question: question_6)
question_7 = Question.create!(about: 'Вопрос про автора',body: 'Назовите автора произведения, которое принцесса Диана держит в руках', format: 'Имя и фамилия',answer_desc: 'Диана держит на руках наследника – маленького принца, поэтому загаданное произведение это «Маленький принц». Автор Антуан де  Сент-Экзюпери', score: 1, stage: 1, game: presentation_game)
Answer.create!(body: 'Антуан де Сент-Экзюпери', question: question_7)
Image.create(question: question_7, attachment: File.new(dir2 + "game_1_7.png"))
question_8 = Question.create!(about: 'Вопрос про актеров',body: 'Какой книжный герой, объединяет всех этих актеров', format: 'Фамилия',answer_desc: 'Все три актера играли роль Фандорина в фильмах «Азазель», «Турецкий гамбит» и «Статский советник»', score: 2, stage: 1, game: presentation_game)
Answer.create!(body: 'Фандорин', question: question_8)
Image.create(question: question_8, attachment: File.new(dir2 + "game_1_8.jpg"))
question_9 = Question.create!(about: 'Вопрос про мрак',body: 'Если это не мрак, то значит, это точно загаданный персонаж', format: 'Имя',answer_desc: 'В словах не мрак, прочитанных наоборот, получается имя Кармен', score: 1, stage: 1, game: presentation_game)
Answer.create!(body: 'Кармен', question: question_9)
question_10 = Question.create!(about: 'Вопрос про символ',body: 'Назовите героя, который утверждает, что превосходит некий символ, который можно найти в цифирном ряду клавиатуры', format: 'Имя',answer_desc: 'Карлсон утверждал , что он же лучше собаки. А собачка - @- это как раз тот символ, который находится на клавиатуре на кнопках цифрового ряда', score: 2, stage: 1, game: presentation_game)
Answer.create!(body: 'Карлсон', question: question_10)
# 2-ой тур
question_11 = Question.create!(about: 'Вопрос про гения',body: 'Какой гений частного сыска заканчивается также, как и название женской сумочки?', format: 'Имя Фамилия',answer_desc: 'Эркюль (Пуаро) – детектив, ридикюль – женская сумочка', score: 1, stage: 2, game: presentation_game)
Answer.create!(body: 'Эркюль Пуаро', question: question_11)
question_12 = Question.create!(about: 'Вопрос про супруга',body: 'Законным супругом этой героини могли бы стать и «родственник животного, способного удушить корыстолюбием», и «шпион, работающий под прикрытием»', format: 'Имя',answer_desc: 'Родственник животного, способного удушить корыстолюбием - это перефразированное выражение – ЖАБА душит, Шпион под прикрытием – КРОТ.', score: 2, stage: 2, game: presentation_game)
Answer.create!(body: 'Дюймовочка', question: question_12)
question_13 = Question.create!(about: 'Вопрос про героя',body: '6 января 2017 года поможет назвать героя одного из известных произведений мировой литературы', format: 'Имя',answer_desc: 'Пятница- герой произведения Д.Дефо «Робинзон Крузо»', score: 3, stage: 2, game: presentation_game)
Answer.create!(body: 'Пятница', question: question_13)
question_14 = Question.create!(about: 'Вопрос про миф',body: 'Нота губ - это герой многочисленных мифов и преданий, а также главный персонаж трагедии конца 18 – начала 19 века', format: 'Имя',answer_desc: 'Нота – фа, губ – уст.', score: 1, stage: 2, game: presentation_game)
Answer.create!(body: 'Фауст', question: question_14)
question_15 = Question.create!(about: 'Вопрос про город',body: 'Кыргызстанский город, в котором последние три буквы заменены витамином аскорбиновой кислоты - это известный персонаж. Какой?', format: 'Имя',answer_desc: 'Город Кара-Балта без 3х последних букв + витамин С= Карабас', score: 2, stage: 2, game: presentation_game)
Answer.create!(body: 'Карабас', question: question_15)
question_16 = Question.create!(about: 'Вопрос про снаряд',body: 'Этот персонаж начинается со спортивного снаряда, состоящего из 16 перьев, а заканчивается Первым каналом до 2002 года', format: 'Имя полностью',answer_desc: 'Волан + орт', score: 2, stage: 2, game: presentation_game)
Answer.create!(body: 'Волан де Морт', question: question_16)
question_17 = Question.create!(about: 'Вопрос про пару',body: 'Постельная принадлежность, в которой вместо предлога находится то, из пары чего приходится предпочитать не самое большое', format: 'Имя',answer_desc: 'Подушка, в которой предлог ПО заменен словом «Зол», так как именно из двух ЗОЛ приходиться выбирать меньшее', score: 1, stage: 2, game: presentation_game)
Answer.create!(body: 'Золушка', question: question_17)
question_18 = Question.create!(about: 'Вопрос про литературу',body: 'Для каких двух знаменитых персонажей европейской литературы общей является водная артерия, известна нам по слабо звучащему советскому роману', format: 'Имя и Имя',answer_desc: 'Слабо звучащий советский роман – Тихий Дон.', score: 2, stage: 2, game: presentation_game)
Answer.create!(body: 'Дон Кихот и Дон Жуан', question: question_18)
question_19 = Question.create!(about: 'Вопрос про утописта',body: 'Если в безмолвном герое-«утописте» русской литературы убрать первое слово волшебного восточного заклинания, но добавить в оставшееся слово букву, с которой начинается «капюшоновый разбойник», то вы назовете сказочную героиню.', format: 'Имя',answer_desc: 'Безмолвный «Утопист» русской литературы – Герасим («Муму») – СИМ – первая часть восточного заклинания (сим-сим)+ буква Р – (капюшоновый разбойник – Робин Гуд)', score: 1, stage: 2, game: presentation_game)
Answer.create!(body: 'Герда', question: question_19)
question_20 = Question.create!(about: 'Вопрос про песню',body: 'Герой, про которого поется, что он находится и здесь и поодаль', format: 'Имя',answer_desc: 'Фигаро тут, Фигаро там', score: 2, stage: 2, game: presentation_game)
Answer.create!(body: 'Фигаро', question: question_20)

# Сиды игр
game1  = Game.create(game_image: Rails.root.join("app/assets/images/game_images/1.jpg").open,  title: 'Beginner 1',    desc: 'Описание 1',  level_id: 1, paid: true, active: true, theme: themes.sample)
question1 = Question.create!(about: 'Вопрос про прыгательные аттракционы', body: 'В основе названия некого прыгательного аттракциона лежит имя именно этого героя', format: 'Имя',answer_desc: 'Прыгалка тарзанка - в ее названии явно есть имя Тарзан', score: 1, stage: 1, game: game1)
Answer.create!(body: 'Тарзан', question: question1)
question2 = Question.create!(about: 'Вопрос про прыгательные аттракционы', body: 'В основе названия некого прыгательного аттракциона лежит имя именно этого героя', format: 'Имя',answer_desc: 'Прыгалка тарзанка - в ее названии явно есть имя Тарзан', score: 1, stage: 2, game: game1)
Answer.create!(body: 'Тарзан', question: question2)
game2  = Game.create(game_image: File.new(dir2 + "2.jpg"),  title: 'Beginner 2',    desc: 'Описание 2',  level_id: 1, paid: true, active: true, theme: themes.sample)
game3  = Game.create(game_image: File.new(dir2 + "3.jpg"),  title: 'Beginner 3',    desc: 'Описание 3',  level_id: 1, paid: true, active: true, theme: themes.sample)
game4  = Game.create(game_image: File.new(dir2 + "4.jpg"),  title: 'New Wave 4',    desc: 'Описание 4',  level_id: 2, paid: true, active: true, theme: themes.sample)
game5  = Game.create(game_image: File.new(dir2 + "5.jpg"),  title: 'New Wave 5',    desc: 'Описание 5',  level_id: 2, paid: true, active: true, theme: themes.sample)
game6  = Game.create(game_image: File.new(dir2 + "6.jpg"),  title: 'New Wave 6',    desc: 'Описание 6',  level_id: 2, paid: true, active: true, theme: themes.sample)
game7  = Game.create(game_image: File.new(dir2 + "7.jpg"),  title: 'Old School 7',  desc: 'Описание 7',  level_id: 3, paid: true, active: true, theme: themes.sample)
game8  = Game.create(game_image: File.new(dir2 + "8.jpg"),  title: 'Old School 8',  desc: 'Описание 8',  level_id: 3, paid: true, active: true, theme: themes.sample)
game9  = Game.create(game_image: File.new(dir2 + "9.jpg"),  title: 'Old School 9',  desc: 'Описание 9',  level_id: 3, paid: true, active: true, theme: themes.sample)
# детские игры
3.times do
  game10 = Game.create(game_image: File.new(dir2 + "10.jpg"), title: 'Детские 10',    desc: 'Описание 10', level_id: 4, paid: true, active: true, theme: themes.sample)
end
# корпоративные игры
3.times do
  game13 = Game.create(game_image: File.new(dir2 + "is.jpg"), title: 'Корпорат 13',   desc: 'Описание 13', level_id: 5, paid: true, active: true, theme: themes.sample)
end

# Бесплатные игры
3.times do
  game16 = Game.create(game_image: File.new(dir2 + "is.jpg"), title: 'Беспл 16 Bg', desc: 'Описание 16', level_id: 1, paid: false, theme: themes.sample)
end


# Лучшие игры
  game19 = Game.create(game_image: File.new(dir2 + "is.jpg"), title: 'Лучш 19', desc: 'Описание 19', paid: nil, active: true, theme: themes.sample)
  game20 = Game.create(game_image: File.new(dir2 + "is.jpg"), title: 'Лучш 20', desc: 'Описание 20', paid: nil, active: true, theme: themes.sample)
  game21 = Game.create(game_image: File.new(dir2 + "is.jpg"), title: 'Лучш 21', desc: 'Описание 21', paid: nil, theme: themes.sample)

gamerating1 = GameRating.create(game: game19, rating: 8)
gamerating2 = GameRating.create(game: game19, rating: 9)
gamerating3 = GameRating.create(game: game19, rating: 10)
gamerating4 = GameRating.create(game: game20, rating: 10)
gamerating5 = GameRating.create(game: game21, rating: 9)
gamerating6 = GameRating.create(game: game21, rating: 10)
gamerating7 = GameRating.create(game_id: 1,   rating: 10)

# Создание новостей
11.times do
  post = Post.create(logo: File.new(dir + "logo.png"),
                     title: Faker::Pokemon.name, url: "https://www.facebook.com/ProektKlassiki/",
                     body: Faker::Lorem.paragraph
  )
  post.images = [
      File.new(dir3 + "3.jpg"),
      File.new(dir3 + "4.jpg"),
      File.new(dir3 + "5.jpg"),
      File.new(dir3 + "6.jpg"),
      File.new(dir3 + "7.jpg"),
      File.new(dir3 + "8.jpg"),
      File.new(dir3 + "9.jpg"),
      File.new(dir3 + "10.jpg"),
      File.new(dir3 + "11.jpg"),
      File.new(dir3 + "12.jpg")
  ]
  post.save
end

post = Post.create(logo: File.new(dir + "logo.png"),
                   title: "Первая ТУРНИРНАЯ игра 2017-го лета - 8 июня в 19:00!", url: "https://www.facebook.com/ProektKlassiki/",
                   body: "Все вкусы мороженного Pingui gelato в одной игре! От фисташки до бубль-гума, от сникерса до голубой лазури!
Пусть впервые за пять лет турнир начнется легко и мега-вкусно!
А с радугой вкусов мороженного вы можете познакомиться либо на страничке Pingui в фейсбук, либо в инстаграм @pinguikg
Регистрация на первую игру летнего турнира - открыта!
До встречи на игре!"
)
post.images = [
    File.new(dir3 + "3.jpg"),
    File.new(dir3 + "4.jpg"),
    File.new(dir3 + "5.jpg"),
    File.new(dir3 + "6.jpg")
]
post.save

post = Post.create(logo: File.new(dir + "logo.png"),
                   title: "Итоги игры 'Украшения'", url: "https://www.facebook.com/ProektKlassiki/",
                   body: "С удовольствием мы отыграли задания про всевозможные украшения и благодаря Lady Accessories победители как самой игры, так и блиц-вопросов стали богаче на изящные украшения в стильных коробочках и сертификаты с приятной суммой, которые можно будет обменять на любые аксессуары в магазинах нашего модного партнера!"
)
post.images = [
    File.new(dir3 + "3.jpg"),
    File.new(dir3 + "4.jpg"),
    File.new(dir3 + "5.jpg"),
    File.new(dir3 + "6.jpg")
]
post.save

# Создание турнирных игр
tournament = Tournament.create(tournament_image: Rails.root.join('app/assets/images/bg.png').open, title: 'Летний турнир', description: "Турнир, посвященный пятилетию игры 'Московские Классики'", active: true)
tournament_game_1 = Game.create(game_image: File.new(dir2 + "is.jpg"), title: 'Первая игра турнира', desc: Faker::Lorem.sentence, paid: false, active: true, tournament: tournament, theme: themes.sample)
tournament_game_2 = Game.create(game_image: File.new(dir2 + "is.jpg"), title: 'Турнирная игра 2', desc: Faker::Lorem.sentence, paid: false, active: true, tournament: tournament, theme: themes.sample)
tournament_game_3 = Game.create(game_image: File.new(dir2 + "is.jpg"), title: 'Турнирная игра 3', desc: Faker::Lorem.sentence, paid: false, active: true, tournament: tournament, theme: themes.sample)

# Игры для личного кабинета
4.times do
  UserGame.create(user_id: 2, game_id: 1, total_score: 17, total_time: 150.133, stage1_start_time: Time.now, stage2_start_time: Time.now, stage1_finish_time: Time.now, stage2_finish_time: Time.now)
end
4.times do
  UserGame.create(user_id: 1, game_id: 2, total_score: 14, total_time: 154.143, stage1_start_time: Time.now, stage2_start_time: Time.now, stage1_finish_time: Time.now, stage2_finish_time: Time.now)
end

# Контентные страницы
about =       Page.create(title: "О нас",          content: 'Московские классики - это интеллектуально-развлекательная игра, в основе которой лежит своеобразный принцип отгадывания оригинальных логических заданий.', active: true, url: "about")
history =     Page.create(title: "История",        content: Faker::Lorem.paragraph, active: true, url: "history")
partnership = Page.create(title: "Сотрудничество", content: Faker::Lorem.paragraph, active: true, url: "partnership")

#Турнирные игры
Game.create(game_image: File.new(dir2 + "book_st.jpg"), title: 'Турнирная игра 1', desc: 'Описание турнирной игры', level_id: 1, paid: false, active: true, theme: themes.sample, start_time: (2.days.from_now), tournament: Tournament.last)
Game.create(game_image: File.new(dir2 + "book_st.jpg"), title: 'Турнирная игра 2', desc: 'Описание турнирной игры', level_id: 1, paid: false, active: true, theme: themes.sample, start_time: (3.days.from_now), tournament: Tournament.last)