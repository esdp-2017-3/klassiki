#Сиды необходимые для тестов
admin = User.create!(email: 'admin@example.com', password: 'password', role: 'admin', alias: 'Admin', confirmed_at: DateTime.now)
user = User.create!(email: 'user@example.com', password: 'password', role: 'none', alias: 'User', confirmed_at: DateTime.now)
user_delete = User.create!(email: 'user_delete@example.com', password: 'password', role: 'none', alias: 'User delete', active: false, confirmed_at: DateTime.now)

#Cиды использующиеся для тестов Турнира
5.times do
  Tournament.create([{title: Faker::HarryPotter.character, description: Faker::HarryPotter.quote}])
end
#Сиды использующиеся для тестов Игр


Level.create!(name: 'Beginner',
              description: 'Beginner - игры, сложность которых является минимальной. Задания в играх рассчитаны на игроков,которые только знакомятся с особенностью вопросов игры.')
Level.create!(name: 'New Wave',
              description: 'New Wave level - игры средней сложности, предназначенные для игроков, которые уже поняли принцип построения и отгадывания вопросов, но игровой опыт еще не столь велик, чтобы перейти к играм сложных тем и вопросов.')
Level.create!(name: 'Old School',
              description: 'Old School level - в основу игр данного уровня легли серьезные исторические, культурные, политические темы, призывающие игроков воспользоваться не только логическим мышлением, но и применить свои знания по теме игры. Задания рассчитаны на игроков, которые уже сыграли ни одну игру данного проекта и готовы к нестандартным постановкам вопросов и к долгим поискам ответов.')
Level.create!([{name: 'Детские'},
               {name: 'Корпоративные'}])

Game.create(title:'Игра для удаления', desc: Faker::Pokemon.name, level_id: 1, id: 1)
10.times do
  Game.create(title: 'game', desc: Faker::Pokemon.name, level_id: 1)
end
# Сиды для тестов  Новостей
2.times do
  post = Post.create(
      title: 'test_news',
      body: 'desc_news',
  )
  post.images = [
      Rails.root.join("app/assets/images/postimages/ruby-logo_small.png").open,
      Rails.root.join("app/assets/images/postimages/ruby-logo_small.png").open
  ]
  post.save
end
10.times do
  post = Post.create(
      title: 'test_news_111',
      body: 'desc_news_111',
      count: 5
  )
  post.images = [
      Rails.root.join("app/assets/images/postimages/ruby-logo_small.png").open,
      Rails.root.join("app/assets/images/postimages/ruby-logo_small.png").open
  ]
  post.save
end
# Сиды для тестов Вопроса

game = Game.create(title:'Test Game', desc: 'Test Game Desc', level_id: 1)
Question.create!(body: 'Question to test', format: Faker::Lorem.sentence, answer_desc: Faker::Lorem.sentence, score: [1, 2, 3].sample, stage: 1, game: game)

#Тест на удаление картинки вопроса
question_for_destroy = Question.create!(body: 'Question for destroy', format: Faker::Lorem.sentence, answer_desc: Faker::Lorem.sentence, score: [1, 2, 3].sample, stage: 1, game: game)

image_for_destroy = Image.create!(
  question: question_for_destroy,
  attachment: Rails.root.join('app', 'assets', 'images', 'test1.jpg').open
)

# image_for_destroy_2 = Image.create!(
#   question: question_for_destroy,
#   attachment: Rails.root.join('public', 'uploads', 'image', 'attachment', 'test', 'test2.jpeg').open
# )

# Сиды для тестов Игрового Процесса

level = Level.create(name: 'test level')
game = Game.create!(title: 'test game', level: level, desc: "test game description")
9.times do
  question = Question.create!(about: Faker::Lorem.sentence, body: Faker::Lorem.sentence, format: Faker::Lorem.sentence ,answer_desc: Faker::Lorem.sentence, score: [1, 2, 3].sample, stage: 1, game: game)
  2.times do
    Answer.create!(body: Faker::Book.title, question: question)
  end
  question = Question.create!(about: Faker::Lorem.sentence, body: Faker::Lorem.sentence, format: Faker::Lorem.sentence ,answer_desc: Faker::Lorem.sentence, score: [1, 2, 3].sample, stage: 2, game: game)
  2.times do
    Answer.create!(body: Faker::Book.title, question: question)
  end
end
question_1 = Question.create!(about: 'Вопрос про прыгательный аттракцион', body: 'В основе названия некого прыгательного аттракциона лежит имя именно этого героя', format: 'Имя',answer_desc: 'Прыгалка тарзанка - в ее названии явно есть имя Тарзан', score: 1, stage: 1, game: game)
Answer.create!(body: 'Тарзан', question: question_1)
question_2 = Question.create!(about: 'Вопрос про город', body: 'Кыргызстанский город, в котором последние три буквы заменены витамином аскорбиновой кислоты - это известный персонаж. Какой?', format: 'Имя',answer_desc: 'Город Кара-Балта без 3х последних букв + витамин С= Карабас', score: 2, stage: 2, game: game)
Answer.create!(body: 'Карабас', question: question_2)

# Тесты для добавления вопросов в игру

8.times do |counter|
  Question.create!(body: Faker::Lorem.sentence, format: Faker::Lorem.sentence ,answer_desc: Faker::Lorem.sentence, score: [1, 2, 3].sample, stage: (counter > 3) ? 1 : 2)
end

# Тесты для проверки статистики

GameRating.create(user_id: 1, game_id: question_1.id, rating: 8)
GameRating.create(user_id: 1, game_id: question_2.id, rating: 7)
QuestionBest.create(user_id: 1, question_id: question_1.id)
QuestionBest.create(user_id: 1, question_id: question_2.id)
QuestionWorst.create(user_id: 1, question_id: question_1.id)
QuestionWorst.create(user_id: 1, question_id: question_2.id)


# Сиды для проверки игр на главной
game1  = Game.create(game_image: Rails.root.join("app/assets/images/game_images/1.jpg").open,  title: 'Beginner 1',    desc: 'Описание 1',  level_id: 1, paid: true)
game2  = Game.create(game_image: Rails.root.join("app/assets/images/game_images/2.jpg").open,  title: 'Beginner 2',    desc: 'Описание 2',  level_id: 1, paid: true)
game3  = Game.create(game_image: Rails.root.join("app/assets/images/game_images/3.jpg").open,  title: 'Beginner 3',    desc: 'Описание 3',  level_id: 1, paid: true)
game4  = Game.create(game_image: Rails.root.join("app/assets/images/game_images/4.jpg").open,  title: 'New Wave 4',    desc: 'Описание 4',  level_id: 2, paid: true)
game5  = Game.create(game_image: Rails.root.join("app/assets/images/game_images/5.jpg").open,  title: 'New Wave 5',    desc: 'Описание 5',  level_id: 2, paid: true)
game6  = Game.create(game_image: Rails.root.join("app/assets/images/game_images/6.jpg").open,  title: 'New Wave 6',    desc: 'Описание 6',  level_id: 2, paid: true)
game7  = Game.create(game_image: Rails.root.join("app/assets/images/game_images/7.jpg").open,  title: 'Old School 7',  desc: 'Описание 7',  level_id: 3, paid: true)
game8  = Game.create(game_image: Rails.root.join("app/assets/images/game_images/8.jpg").open,  title: 'Old School 8',  desc: 'Описание 8',  level_id: 3, paid: true)
game9  = Game.create(game_image: Rails.root.join("app/assets/images/game_images/9.jpg").open,  title: 'Old School 9',  desc: 'Описание 9',  level_id: 3, paid: true)
game10 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/10.jpg").open, title: 'Детские 10',    desc: 'Описание 10', level_id: 4, paid: true)
game11 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Детские 11',    desc: 'Описание 11', level_id: 4, paid: true)
game12 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Детские 12',    desc: 'Описание 12', level_id: 4, paid: true)
game13 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Корпорат 13',   desc: 'Описание 13', level_id: 5, paid: true)
game14 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Корпорат 14',   desc: 'Описание 14', level_id: 5, paid: true)
game15 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Корпорат 15',   desc: 'Описание 15', level_id: 5, paid: true)

# Бесплатные игры
game16 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Беспл 16 Bg', desc: 'Описание 16', level_id: 1, paid: false)
game17 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Беспл 17 NW', desc: 'Описание 17', level_id: 2, paid: false)
game18 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Беспл 18 OS', desc: 'Описание 17', level_id: 3, paid: false)


# Лучшие игры
game19 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Лучш 19', desc: 'Описание 19', paid: nil)
game20 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Лучш 20', desc: 'Описание 20', paid: nil)
game21 = Game.create(game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Лучш 21', desc: 'Описание 21', paid: nil)


gamerating1 = GameRating.create(game: game19, rating: 8)
gamerating2 = GameRating.create(game: game19, rating: 9)
gamerating3 = GameRating.create(game: game19, rating: 10)
gamerating4 = GameRating.create(game: game20, rating: 10)
gamerating5 = GameRating.create(game: game21, rating: 9)
gamerating6 = GameRating.create(game: game21, rating: 10)
gamerating7 = GameRating.create(game_id: 1,   rating: 10)

demo_tournament = Tournament.create(tournament_image: Rails.root.join("app/assets/images/logo.png").open, title: 'Название ближайшего турнира', description: 'Описание турнира. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, porro tenetur rerum praesentium. Voluptate suscipit odit similique, eum, commodi possimus natus quidem doloribus laborum debitis, temporibus et quam inventore dolorem!')
