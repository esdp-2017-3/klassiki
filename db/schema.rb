# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170627132811) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"

  create_table "achievements", force: :cascade do |t|
    t.string   "title"
    t.integer  "tournament_id"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["tournament_id"], name: "index_achievements_on_tournament_id", using: :btree
    t.index ["user_id"], name: "index_achievements_on_user_id", using: :btree
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  end

  create_table "answers", force: :cascade do |t|
    t.string   "body"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["question_id"], name: "index_answers_on_question_id", using: :btree
  end

  create_table "audios", force: :cascade do |t|
    t.string   "attachment"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["question_id"], name: "index_audios_on_question_id", using: :btree
  end

  create_table "game_ratings", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "game_id"
    t.integer  "rating"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_id"], name: "index_game_ratings_on_game_id", using: :btree
    t.index ["user_id"], name: "index_game_ratings_on_user_id", using: :btree
  end

  create_table "games", force: :cascade do |t|
    t.string   "title"
    t.datetime "start_time"
    t.integer  "tournament_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "level_id"
    t.boolean  "paid",          default: false
    t.text     "desc"
    t.string   "game_image"
    t.boolean  "active",        default: false
    t.integer  "theme_id"
    t.index ["level_id"], name: "index_games_on_level_id", using: :btree
    t.index ["theme_id"], name: "index_games_on_theme_id", using: :btree
    t.index ["tournament_id"], name: "index_games_on_tournament_id", using: :btree
  end

  create_table "images", force: :cascade do |t|
    t.string   "attachment"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["question_id"], name: "index_images_on_question_id", using: :btree
  end

  create_table "levels", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "description"
  end

  create_table "pages", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.boolean  "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "url"
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.text     "images"
    t.integer  "count",      default: 0, null: false
    t.string   "logo"
    t.string   "url"
  end

  create_table "question_bests", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["question_id"], name: "index_question_bests_on_question_id", using: :btree
    t.index ["user_id"], name: "index_question_bests_on_user_id", using: :btree
  end

  create_table "question_worsts", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["question_id"], name: "index_question_worsts_on_question_id", using: :btree
    t.index ["user_id"], name: "index_question_worsts_on_user_id", using: :btree
  end

  create_table "questions", force: :cascade do |t|
    t.text     "body"
    t.string   "answer_desc"
    t.string   "format"
    t.integer  "score"
    t.integer  "stage"
    t.integer  "game_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "about"
    t.index ["game_id"], name: "index_questions_on_game_id", using: :btree
  end

  create_table "themes", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tournaments", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.text     "description"
    t.string   "tournament_image"
    t.boolean  "active",           default: false
    t.integer  "games_qty"
  end

  create_table "user_answers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.integer  "user_game_id"
    t.string   "body"
    t.boolean  "right"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["question_id"], name: "index_user_answers_on_question_id", using: :btree
    t.index ["user_game_id"], name: "index_user_answers_on_user_game_id", using: :btree
    t.index ["user_id"], name: "index_user_answers_on_user_id", using: :btree
  end

  create_table "user_games", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "game_id"
    t.datetime "stage1_start_time"
    t.datetime "stage2_start_time"
    t.datetime "stage1_finish_time"
    t.datetime "stage2_finish_time"
    t.integer  "total_score"
    t.float    "total_time"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["game_id"], name: "index_user_games_on_game_id", using: :btree
    t.index ["user_id"], name: "index_user_games_on_user_id", using: :btree
  end

  create_table "user_providers", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_providers_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "role",                   default: "none"
    t.string   "alias"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "provider"
    t.string   "uid"
    t.boolean  "active",                 default: true
    t.string   "avatar"
    t.string   "ban"
    t.datetime "ban_time"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "achievements", "tournaments"
  add_foreign_key "achievements", "users"
  add_foreign_key "answers", "questions"
  add_foreign_key "audios", "questions"
  add_foreign_key "games", "themes"
  add_foreign_key "games", "tournaments"
  add_foreign_key "images", "questions"
  add_foreign_key "question_bests", "questions"
  add_foreign_key "question_bests", "users"
  add_foreign_key "question_worsts", "questions"
  add_foreign_key "question_worsts", "users"
  add_foreign_key "questions", "games"
  add_foreign_key "user_answers", "questions"
  add_foreign_key "user_answers", "user_games"
  add_foreign_key "user_answers", "users"
  add_foreign_key "user_games", "games"
  add_foreign_key "user_games", "users"
  add_foreign_key "user_providers", "users"
end
