ActiveAdmin.register_page 'Dashboard' do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    columns do
      column do
        panel 'Игра (Сколько раз была сыграна)' do
          ul id: 'game' do
            counter = 0
            Game.game_sort.each do |game|
              counter += 1
              if counter < 6
                li "#{game.title} (#{game.user_games.count})"
              else
                li id: 'rating_show' do
                  link_to('Отобразить остальное', statistics_count_display_path, remote: true)
                end
                break
              end
            end
          end
        end
      end

      column do
        panel 'Новости (Количество просмотренных)' do
          ul id: 'post' do
            counter = 0
            Post.post_sort.each do |post|
              counter += 1
              if counter < 6
                li "#{post.title} (#{post.count})"
              else
                li id: 'rating_show' do
                  link_to('Отобразить остальное', statistics_post_display_path, remote: true)
                end
                break
              end
            end
          end
        end
      end
    end

    columns do
      column do
        panel 'Рейтинг игр' do
          ul id: 'rating' do
            counter = 0
            Game.sort_array_by_rating.each do |game|
              counter += 1
              if counter < 6
                li "#{game.title} (#{game.rating})"
              else
                li id: 'rating_show' do
                  link_to('Отобразить остальное', statistics_game_display_path, remote: true)
                end
                break
              end
            end
          end
        end
      end

      column do
        panel 'Лучшие вопросы:' do
          ul id: 'best' do
            counter = 0
            Question.sort_array_by_best.each do |question|
              counter += 1
              if counter < 6
                li "#{question.body} (#{question.question_bests.count})"
              else
                li id: 'best_show' do
                  link_to('Отобразить остальное', statistics_best_display_path, remote: true)
                end
                break
              end
            end
          end
        end
      end

      column do
        panel 'Тяжелые вопросы:' do
          ul id: 'worst' do
            counter = 0
            Question.sort_array_by_worst.each do |question|
              counter += 1
              if counter < 6
                li "#{question.body} (#{question.question_worsts.count})"
              else
                li id: 'worst_show' do
                  link_to('Отобразить остальное', statistics_worst_display_path, remote: true)
                end
                break
              end
            end
          end
        end
      end
    end
  end
end