ActiveAdmin.register Game do
  permit_params :game_image, :desc, :theme_id, :tournament_id, :title, :level_id, :start_time, :paid, :active, question_ids: []

  config.clear_action_items!
  action_item('Создать игру', only: :index) do
    link_to 'Создать Игру' , new_admin_game_path
  end

  action_item('Изменить Игру', only: :show) do
    link_to('Изменить Игру' , edit_admin_game_path(game))
  end

  action_item('Удалить игру', only: :show) do
    link_to('Удалить Игру' , admin_game_path(game), method: :delete, data: { confirm: 'Вы уверены?' })
  end

  index do
    selectable_column
    column :title
    column :game_image do |game|
      image_tag(game.game_image, size: '70x100') unless game.game_image.blank?
    end
    column :tournament
    column :theme
    column :level
    column :paid
    column :active
    actions
  end

#Custom Show
  show do
    panel 'Информация' do
      tabs do
        tab 'Об игре' do
          attributes_table_for game do
            row :title
            row :game_image do |game|
              image_tag(game.game_image,  size: '250x400') unless game.game_image.blank?
            end
            row :desc do |game|
              raw game.desc
            end
            row :start_time
            row :tournament
            row :created_at
            row :updated_at
            row :level
            row :theme
            row :paid

          end #Attributes
        end #Tab 1

        tab 'Вопросы' do
          table_for game.questions do
            column :body
          end
        end #Tab 2

      end #Tabs
    end #Panel
  end #Show

#Custom Create
  form title: 'Создать Игру' do |f|
    f.inputs 'Информация' do
      f.input :game_image, as: :file
      f.input :title
      f.input :desc
      f.input :paid, as: :boolean
      f.input :active
      f.input :level
      f.input :theme
      f.input :tournament
      f.input :start_time
      f.inputs 'Этап 1' do
        if game != nil
          f.input :question_ids, label: 'Вопросы',
                  as: :check_boxes,
                  collection: Question.where(game_id: nil, stage: 1).or(Question.where(game_id: game.id, stage: 1)).pluck(:body, :id),
                  multiple: true
        else
          f.input :question_ids, label: 'Вопросы',
                  as: :check_boxes,
                  collection: Question.where(game_id: nil, stage: 1).pluck(:body, :id),
                  multiple: true
        end
      end
      f.inputs 'Этап 2' do
        if game != nil
          f.input :question_ids, label: 'Вопросы',
                  as: :check_boxes,
                  collection: Question.where(game_id: nil, stage: 2).or(Question.where(game_id: game.id, stage: 2)).pluck(:body, :id),
                  multiple: true
        else
          f.input :question_ids, label: 'Вопросы',
                  as: :check_boxes,
                  collection: Question.where(game_id: nil, stage: 2).pluck(:body, :id),
                  multiple: true
        end
      end
    end
    f.actions
  end
end