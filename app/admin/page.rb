ActiveAdmin.register Page do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :title, :content, :active, :url
#
  index do
    selectable_column
    id_column
    column :title do |page|
      raw page.title
    end
    column :content do |page|
      raw page.content
    end
    column :active
    column :url do |page|
      raw page.url
    end
    actions
  end


end
