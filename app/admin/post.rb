ActiveAdmin.register Post do
    permit_params :title, :logo, :body, images: []

    form do |f|
      f.inputs do
        f.input :title
        f.input :url
        f.input :logo, as: :file, hint: f.post.logo.present? \
         ? image_tag(f.post.logo.url(:thumb))
         : content_tag(:span, 'Лого не загружено')

        f.input :logo_cache, as: :hidden
        f.input :body
        f.input :images, as: :file, input_html: { multiple: true }
        post.images.each do |i|
          li do
            image_tag(i.url(:thumb))
          end
        end
      end
      f.actions
    end

    index do
      selectable_column
      id_column
      column :body do |post|
         raw post.body
      end
      column :url do |post|
        raw post.url
      end
      column :logo do |post|
        image_tag(post.logo, height: '70px') unless post.logo.blank?
      end
      column :title do |post|
        link_to post.title, admin_post_path(post)
      end
      actions
    end

    show do
      attributes_table do
        row :body do
          raw post.body
        end
        row :logo do |post|
          image_tag(post.logo,  size: '250x400') unless post.logo.blank?
        end
        row :title
        row :url
        row :images do
          ul do
            post.images.each do |image|
              li do
                image_tag(image.url)
              end
            end
          end
        end
      end
    end


end
