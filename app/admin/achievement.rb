ActiveAdmin.register Achievement do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :title, :tournament_id, :user_id
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  index do
    selectable_column
    column :title
    column :tournament
    column :user
    actions
  end

#Custom Show
  show do
    attributes_table do
      row :title
      row :tournament
      row :user
      row :created_at
      row :updated_at
    end
  end #Show

#Custom Create
  form title: 'Создать достижение' do |f|
    f.inputs 'Информация' do
      f.input :title
      f.input :tournament_id, :label => 'Турнир', :as => :select, :collection => Tournament.where.not(id: Achievement.select(:tournament_id)).map{|t| [t.title, t.id]}
      f.input :user_id, :label => 'Member', :as => :select, :collection => User.all.map{|u| [u.alias, u.id]}
    end
    f.actions
  end

end
