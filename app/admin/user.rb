ActiveAdmin.register User do
  actions :all, except: [:destroy]

  controller do
    def update
      if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
      super
    end
  end

  config.clear_action_items!
  action_item('Создать пользователя', only: :index) do
    link_to 'Создать Пользователя' , new_admin_user_path
  end

  action_item('Изменить пользователя', only: :show) do
    link_to('Изменить Пользователя' , edit_admin_user_path(user))
  end

  action_item('Удалить/Восстановить пользователя', only: :show) do
    link_to((user.active ? 'Удалить' : 'Восстановить') , inactive_admin_user_path(user), method: :put)
  end

  permit_params :avatar, :email, :password, :password_confirmation, :role, :alias, :ban
  index do
    column :avatar do |user|
      image_tag(user.avatar, size: '70x100') unless user.avatar.blank?
    end
    column :alias
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :role
    column :active
    column :ban
    actions defaults: true do |user|
      link_to (user.active ? 'Удалить' : 'Восстановить'), inactive_admin_user_path(user), method: :put
    end
  end
  filter :email
  form title: 'Создать Пользователя' do |f|
    f.inputs do
      f.input :avatar, as: :file
      f.input :alias
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :role, as: :radio, collection: {None: "none", Administrator: "admin"}
      f.input :active, label: 'Активный', input_html: { checked: 'checked' }
      f.input :ban, as: :select, label: 'Заблокировать', collection: [
        [ '3 дня', '3 days'],
        ['неделя', 'week'],
        ['месяц', 'month']
      ]
    end
    f.actions
  end

  member_action :inactive, method: :put do
    user = User.find(params[:id])
    message = ""
    if user.active
      user.update(active: false)
      message = "#{user.alias} удалён"
    else
      user.update(active: true)
      message = "#{user.alias} восстановлен"
    end
    redirect_to admin_users_path, notice: message
  end

end