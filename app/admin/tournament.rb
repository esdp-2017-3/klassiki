ActiveAdmin.register Tournament do

permit_params :tournament_image, :title, :description, :active, game_ids: []

  index do
    selectable_column
    id_column
    column :title
    column :tournament_image do |tournament|
      image_tag(tournament.tournament_image, size: '70x100') unless tournament.tournament_image.blank?
    end
    column :created_at
    column :updated_at
    column :description do |tournament|
      raw tournament.description
    end
    column :active
    column :games_qty
    actions
  end

  show do
    attributes_table do
      row :title
      row :tournament_image do |tournament|
        image_tag(tournament.tournament_image,  size: '250x400') unless tournament.tournament_image.blank?
      end
      row :description do |tournament|
        raw tournament.description
      end
      row :active
      row :games_qty
      row :created_at
      row :updated_at
    end
  end

  form title: 'Создать Турнир' do |f|
    f.inputs 'Информация' do
      f.input :title
      f.input :description
      f.input :tournament_image
      f.input :active

      f.inputs 'Игры' do
        if tournament != nil
          f.input :game_ids,
                  label: 'Выберите игры:',
                  as: :check_boxes,
                  collection: Game.where(tournament_id: nil).or(Game.where(tournament_id: tournament.id)).pluck(:title, :id),
                  multiple: true
        else
          f.input :game_ids,
                  label: 'Выберите игры:',
                  as: :check_boxes,
                  collection: Game.where(tournament_id: nil).pluck(:title, :id),
                  multiple: true
        end
      end

    end
    f.actions
  end

end
