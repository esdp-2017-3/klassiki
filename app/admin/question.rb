ActiveAdmin.register Question do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :about, :body, :answer_desc, :format, :score, :stage, :game_id, answers_attributes: [:id, :body, :question_id], images_attributes: [:id, :attachment, :question_id], audios_attributes: [:id, :attachment, :question_id]

  form do |f|
    f.inputs do
      f.input :game
      f.input :stage, as: :radio, collection: [1, 2], include_blank: false
      f.input :score, as: :radio, collection: [1, 2, 3], include_blank: false
      f.input :about
      f.input :body, input_html: {class: 'tinymce'}
      f.has_many :images, new_record: 'Добавить Изображение' do |i|
        i.input :attachment, as: :file
      end
      f.has_many :audios, new_record: 'Добавить аудиофайл' do |i|
        i.input :attachment, as: :file
      end
      f.has_many :answers, new_record: 'Добавить Ответ' do |i|
        i.input :body
      end
      f.input :format
      f.input :answer_desc
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :body do |question|
      link_to ( raw question.body), admin_question_path(question)
    end
    column :game_id do |question|
      question.game.title if question.game
    end
    column :stage do |question|
      question.stage
    end
    column :score do |question|
      question.score
    end
    column :images do |question|
      ul do
        question.images.each do |image|
          li do
            image_tag image.attachment_url(:thumb)
          end
        end
      end
    end
    actions
  end

  show do
    attributes_table do
      row :about
      row :body do
        raw question.body
      end
      row :images do
        ul do
          question.images.each do |image|
            li do
              image_tag image.attachment_url(:thumb)
            end
          end
        end
      end
      row :audios do
        ul do
          question.audios.each do |audio|
            li do
              audio_tag(audio.attachment, controls: true)
            end
          end
        end
      end
      row :answer_desc
      row :format
      row :score
      row :stage
      row :game do
        question.game.title if question.game
      end
      row :answers do
        ul do
          question.answers.each do |answer|
            li do
              answer.body
            end
          end
        end
      end
    end
  end

end
