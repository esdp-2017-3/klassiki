ActiveAdmin.register Theme do

permit_params :name

config.clear_action_items!
action_item('Создать Тематику', only: :index) do
  link_to 'Создать Тематику' , new_admin_theme_path
end

action_item('Изменить Тематику', only: :show) do
  link_to('Изменить Тематику' , edit_admin_theme_path(theme))
end

action_item('Удалить Тематику', only: :show) do
  link_to('Удалить Тематику' , admin_theme_path(theme), method: :delete, data: { confirm: 'Вы уверены?' })
end

form title: 'Создать/Редактировать Тематику' do |f|
  f.inputs do
    f.input :name
  end
  f.actions
end

end
