class Game < ApplicationRecord
  include PgSearch
  paginates_per 10
  mount_uploader :game_image, GameImageUploader
  belongs_to :tournament
  has_many :questions, dependent: :nullify
  has_many :user_games, dependent: :destroy
  has_many :game_ratings, dependent: :destroy
  belongs_to :level
  belongs_to :theme
  validates :title, presence: true
  validates :desc, presence: true
  validates :theme, presence: true

  pg_search_scope :search_for, against: %i(title desc),
                  using: { tsearch: { any_word: true },
                           trigram: { threshold: 0.1 }}


  def time_to_start
    time = self.start_time - Time.current
    Time.at(time).utc.strftime('%m месяцев %d дней %H часов %M минут')
  end

  def is_tournament_game?
    tournament != nil
  end

  def stage_time(stage)
    questions.where(stage: stage).count * 3
  end

  def rating
    game_ratings.length > 0 ? game_ratings.average(:rating).to_f : 0.0
  end

  def self.sort_array_by_rating
    Game.all.select{ |game| game.rating > 0 }.sort_by(&:rating).reverse
  end

  def self.game_sort
    games = Game.all.select{ |game| game.user_games.count != 0 }
    games.sort_by{ |game| game.user_games.count }.reverse
  end

  def random_results
    all.shuffle.take(3)
  end

  def self.best
    Game.all.sort_by(&:rating).reverse.first(3)
  end
end