class Tournament < ApplicationRecord
  has_many :games, dependent: :destroy
  has_many :achievements, dependent: :destroy

  mount_uploader :tournament_image, GameImageUploader
  
  def actual_game
    games.order(:start_time).each do |game|
      @result = game if game.start_time && Time.current < game.start_time + 4200
      break if @result
    end
    @result
  end

  def start_time
    self.actual_game.start_time
  end

  def results
    @results = []
    self.games.each do |game|
      game.user_games.each do |user_game|
        @results << {game: user_game.game.title, user: user_game.user.alias, score: user_game.total_score}
      end
    end
    @total = Hash.new(0)
    @results.each_with_object(@total) do |user|
      @total[user[:user]] += user[:score]
    end
    return @total
  end
end