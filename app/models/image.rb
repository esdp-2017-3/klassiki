class Image < ApplicationRecord
  belongs_to :question
  mount_uploader :attachment, QuestionImageUploader
  validates :attachment, presence: true

end