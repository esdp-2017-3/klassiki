class Question < ApplicationRecord
  belongs_to :game
  has_many :user_answers, dependent: :destroy
  has_many :answers, dependent: :destroy
  has_many :images, dependent: :destroy
  has_many :audios, dependent: :destroy
  has_many :question_bests, dependent: :destroy
  has_many :question_worsts, dependent: :destroy
  accepts_nested_attributes_for :answers, allow_destroy: true
  accepts_nested_attributes_for :images, allow_destroy: true
  accepts_nested_attributes_for :audios, allow_destroy: true
  validates :stage, presence: true, numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 2 }
  validates :score, presence: true, numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 3 }

  def self.sort_array_by_best
    questions = Question.all.select{ |q| q.question_bests.count > 0 }
    questions.sort_by{ |q| q.question_bests.count }.reverse
  end

  def self.sort_array_by_worst
    questions = Question.all.select{ |q| q.question_worsts.count > 0 }
    questions.sort_by{ |q| q.question_worsts.count }.reverse
  end
end
