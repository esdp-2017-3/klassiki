class Page < ApplicationRecord
  validates :url, uniqueness: true, presence: true
end
