class Post < ApplicationRecord
  include PgSearch
  paginates_per 10
  validates_presence_of :title, :body
  mount_uploaders :images, ImageUploader
  mount_uploader :logo, GameImageUploader
  serialize :images, JSON

  pg_search_scope :search_for, against: %i(title body),
                               using: { tsearch: { any_word: true },
                                        trigram: { threshold: 0.1 }}

  def self.post_sort
    Post.where('count > ?', 0).sort_by { |post| post.count }.reverse
  end

end
