class UserAnswer < ApplicationRecord
  belongs_to :user
  belongs_to :question
  belongs_to :user_game

  def right?
    self.right = question.answers.any? { |answer| to_format(answer.body) == to_format(body) } ? true : false
  end

  def to_format(body)
    body.gsub!(/^\s*/, '').gsub!(/\s*$/, '').mb_chars.downcase!.to_s
  end

end
