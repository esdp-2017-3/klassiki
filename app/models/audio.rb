class Audio < ApplicationRecord
  belongs_to :question
  mount_uploader :attachment, QuestionAudioUploader
  validates :attachment, presence: true

end
