class UserGame < ApplicationRecord
  belongs_to :user
  belongs_to :game
  has_many :user_answers, dependent: :destroy

  def stage1_finished?
    stage1_finish_time ? true : false
  end

  def score_count
    user_answers.where(right: true).inject(0){|sum, x| sum + x.question.score }
  end

  def stage_1_questions_accessed?(question, stage)
    user_answers.where(question: question).all? { |answer| answer.body.nil? } && stage1_finish_time.nil? && stage == 1
  end

  def stage_2_questions_accessed?(question, stage)
    user_answers.where(question: question).all? { |answer| answer.body.nil? } && stage2_finish_time.nil? && stage == 2
  end

  def time_count
    (stage1_finish_time - stage1_start_time) + (stage2_finish_time - stage2_start_time)
  end

  def registered
    user ? user.alias : 'Unregistered'
  end

end
