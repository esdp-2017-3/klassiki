class User < ApplicationRecord
  include PgSearch
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:facebook, :google_oauth2, :vkontakte]

  before_save :save_ban_time

  has_many :user_games, dependent: :destroy
  has_many :user_answers, dependent: :destroy
  has_many :game_ratings, dependent: :destroy
  has_many :question_bests, dependent: :destroy
  has_many :question_worsts, dependent: :destroy
  has_many :user_provider, dependent: :destroy
  has_many :achievements, dependent: :destroy

  mount_uploader :avatar, AvatarUploader


  pg_search_scope :search_for, against: %i(alias),
                  using: { tsearch: { any_word: true } }

  def not_finished_game
    user_games.where(stage2_finish_time: nil).last
  end

  def role?(r)
    role.include? r.to_s
  end

  def ban?
    return false unless ban_time
    ban_time > Time.now ? true : false
  end

  private

  def save_ban_time
    if ban
      case ban
        when '3 days' then self.ban_time = Time.now + 3.days
        when 'week' then self.ban_time = Time.now + 7.days
        when 'month' then self.ban_time = Time.now + 30.days
      end
      self.ban = nil
    end
  end
end
