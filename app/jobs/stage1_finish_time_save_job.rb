class Stage1FinishTimeSaveJob < ApplicationJob
  queue_as :default

  def perform(user_game_id)
    user_game = UserGame.find(user_game_id)
    user_game.stage1_finish_time ||= user_game.stage1_start_time + user_game.game.stage_time(1).minutes
    user_game.save!
  end

end
