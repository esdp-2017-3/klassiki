class Stage2StartTimeSaveJob < ApplicationJob
  queue_as :default

  def perform(user_game)
    user_game.stage2_start_time ||= user_game.stage1_finish_time + 10.minutes
    user_game.save!
  end
end
