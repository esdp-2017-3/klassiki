class Stage2FinishTimeSaveJob < ApplicationJob
  queue_as :default

  def perform(user_game)
    user_game.stage2_finish_time ||= user_game.stage2_start_time + user_game.game.stage_time(2).minutes
    user_game.save!
  end
end
