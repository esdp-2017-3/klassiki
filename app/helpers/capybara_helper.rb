module CapybaraHelper

  def create_levels
    FactoryGirl.create(:level, name: 'New Wave',
                       description: 'New Wave level - игры средней сложности, предназначенные для игроков, которые уже поняли принцип построения и отгадывания вопросов, но игровой опыт еще не столь велик, чтобы перейти к играм сложных тем и вопросов.')
    FactoryGirl.create(:level, name: 'Old School',
                       description: 'Old School level - в основу игр данного уровня легли серьезные исторические, культурные, политические темы, призывающие игроков воспользоваться не только логическим мышлением, но и применить свои знания по теме игры. Задания рассчитаны на игроков, которые уже сыграли ни одну игру данного проекта и готовы к нестандартным постановкам вопросов и к долгим поискам ответов.')
    FactoryGirl.create(:level, name: 'Beginner',
                       description: 'Beginner - игры, сложность которых является минимальной. Задания в играх рассчитаны на игроков,которые только знакомятся с особенностью вопросов игры.')
    FactoryGirl.create(:level, name: 'Детские',
                       description: '')
    FactoryGirl.create(:level, name: 'Корпоративные',
                       description: '')
  end

  def tinymce_fill_in name, options = {}
    if page.driver.browser.browser == :chrome || :firefox
      page.driver.browser.switch_to.frame("#{name}_ifr")
      page.find(:css, '#tinymce').native.send_keys(options[:with])
      page.driver.browser.switch_to.default_content
    else
      page.execute_script("tinyMCE.get('#{name}').setContent('#{options[:with]}')")
    end
  end
end