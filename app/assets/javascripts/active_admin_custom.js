
$(document).ready(function() {
    tinymce.init({
        selector: "textarea",
        editor_selector: "tinymce_editor",
        plugins: ['image', 'link', 'media'],
        language: 'ru'
    });
});
