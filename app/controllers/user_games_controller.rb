class UserGamesController < ApplicationController
  before_action :set_user_game, except: [:create, :save_statistic_anwers]
  before_action :game_access, only: [:play, :begin_stage]
  before_action :game_ability, only: [:play, :results]

  def create
    @game = Game.find(params[:game])
    time = @game.is_tournament_game? ? @game.start_time : Time.now
    @user_game = UserGame.new(user: current_or_guest_user, game: @game, stage1_start_time: time)
    @user_game.save
    save_jobs_set(@game, @user_game)
    redirect_to user_game_play_path(@user_game)
  end

  def play
    @stage = (@user_game.stage1_finished? && @user_game.stage2_start_time ? 2 : 1)
    @questions = Question.where(game: @user_game.game, stage: @stage).order(id: :asc)
    @user_answer = UserAnswer.new
    @time_out = time_find_time(@user_game, @stage)
    @time_out_min = time_find_min(@user_game, @stage)
    @time_out_sec = time_find_sec(@user_game, @stage)
  end

  def begin_stage
    @user_game.stage2_start_time ||= Time.now
    @user_game.save
    redirect_to user_game_play_path(@user_game, stage: 2)
  end

  def finish_stage
    if @user_game.stage1_finished? && @user_game.stage2_start_time
      @user_game.stage2_finish_time ||= Time.now
      @user_game.total_time = @user_game.time_count
      stage = [1, 2]
    else
      @user_game.stage1_finish_time ||= Time.now
      stage = 1
    end
    @user_game.total_score = @user_game.score_count
    @user_game.save
    redirect_to user_game_results_path(@user_game, stage: stage)
  end

  def results
    stage = (@user_game.stage2_finish_time ? [1, 2] : 1)
    results_save_jobs_set(@user_game)
    @number_of_rating = *(1..10)
    @questions = Question.where(game: @user_game.game, stage: stage)
    @user_answers = UserAnswer.where(user_game: @user_game)
    @time_for_pause = time_find_pause(@user_game)
    @time_out_min = pause_find_min(@user_game)
    @time_out_sec = pause_find_sec(@user_game)
  end

  def destroy
    @user_game.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Игра была завершена досрочно. Прогресс не был сохранен' }
      format.json { head :no_content }
    end
  end

  def save_statistic_anwers
    user = (user_signed_in?) ? current_user : nil
    params.each do |key, value|
      if key == "best"
        save_question_best(user, value.to_i)
      elsif key == "worst"
        save_question_worst(user, value.to_i)
      end
    end
    save_game_rating(user, params[:game_id], params[:rating])
    redirect_to root_path, notice: 'Спасибо за игру'
  end

  private

  def set_user_game
    @user_game = UserGame.find(params[:id])
  end

  def game_access
    render_404 if @user_game.stage2_finish_time
  end

  def game_ability
    render_404 if cannot? :read, @user_game
  end

  def save_game_rating(user, game_id, rating)
    GameRating.create(user: user, game_id: game_id, rating: rating)
  end

  def save_question_best(user, question_id)
    QuestionBest.create(user: user, question_id: question_id)
  end

  def save_question_worst(user, question_id)
    QuestionWorst.create(user: user, question_id: question_id)
  end

  def time_find_time(user_game, stage)
    time = time_find(user_game, stage)
    '%d:%02d:%02d' % [time / 3600, (time / 60) % 60, time % 60]
  end

  def time_find_min(user_game, stage)
    ((time_find(user_game, stage) / 60) % 60).to_i
  end

  def time_find_sec(user_game, stage)
    (time_find(user_game, stage) % 60).to_i
  end

  def time_find(user_game, stage)
    if stage == 1
      time_out = Time.now - user_game.stage1_start_time
    elsif stage == 2
      time_out = Time.now - user_game.stage2_start_time
    end
    time_out = user_game.game.stage_time(stage) * 60 - time_out
    time_out = 0 if @user_game.game.stage_time(@stage) == 0
    time_out
  end

  def time_find_pause(user_game)
    time = pause_find(user_game)
    '%d:%02d:%02d' % [time / 3600, (time / 60) % 60, time % 60]
  end

  def pause_find_min(user_game)
    ((pause_find(user_game) / 60) % 60).to_i
  end

  def pause_find_sec(user_game)
    (pause_find(user_game) % 60).to_i
  end

  def pause_find(user_game)
    600 - (Time.now - user_game.stage1_finish_time)
  end

  def results_save_jobs_set(user_game)
    unless user_game.stage2_finish_time
      Stage2StartTimeSaveJob.set(wait_until: user_game.stage1_finish_time.utc + 10.minutes).perform_later(user_game)
    end
  end

  def save_jobs_set(game, user_game)
    Stage1FinishTimeSaveJob.set(wait: game.stage_time(1).minutes).perform_later(user_game.id)
    Stage2StartTimeSaveJob.set(wait: game.stage_time(1).minutes + 10.minutes).perform_later(user_game)
    Stage2FinishTimeSaveJob.set(wait: game.stage_time([1,2]).minutes + 10.minutes).perform_later(user_game)
  end

end
