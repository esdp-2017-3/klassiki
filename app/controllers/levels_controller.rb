class LevelsController < ApplicationController

  def show
    @level = Level.find(params[:id])
    @games = @level.games.where(active: true, tournament: nil).order(title: :ASC).page params[:page]
  end
end
