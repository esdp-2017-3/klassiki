class GamesController < ApplicationController
  def index
  	@games = Game.where(active: true).order(title: :ASC).page params[:page]
  end

  def show
    @game = Game.find(params[:id])
  end

  def rank
    @game = Game.find(params[:id])

    @first_results = []
    uniq_ids = []
    @game.user_games.order("created_at ASC").each do |user_game|
      unless uniq_ids.include? user_game.user.id
        if user_game.user.alias != "guest" && user_game.stage2_finish_time != nil
          @first_results << user_game
          uniq_ids << user_game.user.id
        end
      end
    end
  end
end