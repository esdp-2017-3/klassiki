class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :ban?
  helper_method :current_or_guest_user, :guest_user

  def ban?
    if current_user.present? && current_user.ban?
      sign_out current_user
      redirect_to root_path, notice: 'Ваш аккаунт заблокирован'
    end
  end

  def current_ability
    @current_ability ||= Ability.new(current_or_guest_user)
  end

  def authenticate_active_admin_user!
    authenticate_user!
    unless current_user.role?(:admin)
      flash[:alert] = 'У Вас нет прав доступа к данному разделу!'
      redirect_to root_path
    end
  end

  def current_or_guest_user
    if current_user
      if session[:guest_user_id] && session[:guest_user_id] != current_user.id
        guest_user(with_retry = true).reload.try(:destroy)
        session[:guest_user_id] = nil
      end
      current_user
    else
      guest_user
    end
  end

  def render_404
    render file: "#{Rails.root}/public/404.html", status: 404, layout: false
  end

  def guest_user(with_retry = true)
    @cached_guest_user ||= User.find(session[:guest_user_id] ||= create_guest_user.id)
    rescue ActiveRecord::RecordNotFound
    session[:guest_user_id] = nil
    guest_user if with_retry
  end

  private

  def create_guest_user
    u = User.create(alias: 'guest', email: "guest_#{Time.now.to_i}#{rand(100)}@example.com")
    u.confirm
    u.save!(validate: false)
    session[:guest_user_id] = u.id
    u
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:alias])
    devise_parameter_sanitizer.permit(:account_update, keys: [:alias])
  end

  def set_locale
    I18n.locale = params[:locale] if params[:locale].present?
  end

  def default_url_options(options={})
    {locale: I18n.locale}
  end

end
