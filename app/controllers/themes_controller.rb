class ThemesController < ApplicationController
  def index
    @themes = Theme.order(name: :ASC)
  end

  def show
    @theme = Theme.find(params[:id])
    @games = @theme.games.where(active: true, tournament: nil).order(title: :ASC).page params[:page]
  end
end
