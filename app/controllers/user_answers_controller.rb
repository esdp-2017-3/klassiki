class UserAnswersController < ApplicationController

  def create
    @user_game = UserGame.find(params[:user_game_id])
    @user_answer = UserAnswer.new(user_answer_params)
    @user_answer.user = current_or_guest_user
    @user_answer.user_game = @user_game
    @user_answer.right?
    @user_answer.save
    redirect_back(fallback_location: root_path)
  end

  private

  def user_answer_params
    params.require(:user_answer).permit(:body, :question_id)
  end
end
