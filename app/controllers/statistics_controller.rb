class StatisticsController < ApplicationController

  def best_display
    @questions = Question.sort_array_by_best
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Включите JavaScript" }
      format.js {}
    end
  end

  def best_close
    @questions = Question.sort_array_by_best[0..4]
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Включите JavaScript" }
      format.js {}
    end
  end

  def worst_display
    @questions = Question.sort_array_by_worst
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Включите JavaScript" }
      format.js {}
    end
  end

  def worst_close
    @questions = Question.sort_array_by_worst[0..4]
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Включите JavaScript" }
      format.js {}
    end
  end

  def game_display
    @games = Game.sort_array_by_rating
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Включите JavaScript" }
      format.js {}
    end
  end

  def game_close
    @games = Game.sort_array_by_rating[0..4]
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Включите JavaScript" }
      format.js {}
    end
  end

  def post_display
    @posts = Post.post_sort
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Включите JavaScript" }
      format.js {}
    end
  end

  def post_close
    @posts = Post.post_sort.first(5)
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Включите JavaScript" }
      format.js {}
    end
  end

  def count_display
    @games = Game.game_sort
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Включите JavaScript" }
      format.js {}
    end
  end

  def count_close
    @games = Game.game_sort[0..4]
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Включите JavaScript" }
      format.js {}
    end
  end

end
