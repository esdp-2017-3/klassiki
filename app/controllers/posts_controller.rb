class PostsController < ApplicationController
  def index
    @posts = Post.order(created_at: :desc)
  end

  def show
    @post = Post.find(params[:id])
    @post.count = @post.count + 1
    @post.save
  end

  private

  def posts_params
    params.require(:post).permit(:title, :body, images: [])
  end
end
