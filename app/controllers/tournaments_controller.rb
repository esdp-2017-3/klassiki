class TournamentsController < ApplicationController
  def show
    @tournament = Tournament.find(params[:id])
    render_404 unless @tournament.active
    @game = @tournament.actual_game
    @start_time = @game.start_time if @game
  end

  def stat
    @tournament = Tournament.last
    @results = @tournament.results
  end
end