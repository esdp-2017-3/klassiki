class PagesController < ApplicationController
  def main
    @levels = Level.all
    @level_types = Level.where(name: 'Детские').or(Level.where(name: 'Корпоративные'))
    @posts = Post.order(created_at: :desc).page params[:page]
    @tournament = Tournament.where(active: true).last
    @best = Game.where(active: true).best
    @free_games = Game.where(paid: false, active: true, tournament: nil)
    @pages = Page.where(active: true)
  end

  def show
    @page = Page.find_by_url(params[:url])
    render_404 unless @page.active
  end

  def search
    @search = params[:search]
    @games = Game.search_for(@search).where(active: true)
    @posts = Post.search_for(@search)
    @users = User.search_for(@search)
  end
end