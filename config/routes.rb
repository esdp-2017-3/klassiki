Rails.application.routes.draw do
  require 'sidekiq/web'

  root to: 'pages#main'

  get 'tournaments/:id/stat', to: 'tournaments#stat', as: 'tournament_stat'
  ActiveAdmin.routes(self)
  authenticate :user, lambda { |u| u.role?(:admin) } do
    mount Sidekiq::Web => '/sidekiq'
  end
  resources :themes, only: [:index, :show]
  resources :profiles
  resources :levels, only: [:show, :index]
  resources :posts, only: [:show, :index]
  resources :tournaments, only: [:show]
  resources :games, only: [:index, :show] do
    # collection do
    #   get ':id/rank', to: 'games#rank', as: 'rank'
    #   get ':id/play', to: 'user_games#play', as: 'play'
    #   get ':id/results', to: 'user_games#results', as: 'results'
    #   post ':id/begin_stage', to: 'user_games#begin_stage', as: 'begin_stage'
    #   post ':id/finish_stage', to: 'user_games#finish_stage', as: 'finish_stage'
    #   post ':game_id/save_statistic_anwers', to: 'user_games#save_statistic_anwers', as: 'user_game/save_statistic_anwers'
    # end
  end
  resources :user_games, only: [:create, :destroy] do
    resources :user_answers, only: [:create]
  end

  get 'games/:id/rank', to: 'games#rank', as: 'game_rank'
  get 'games/:id/play', to: 'user_games#play', as: 'user_game/play'
  get 'games/:id/results', to: 'user_games#results', as: 'user_game/results'
  post 'games/:id/begin_stage', to: 'user_games#begin_stage', as: 'user_game/begin_stage'
  post 'games/:id/finish_stage', to: 'user_games#finish_stage', as: 'user_game/finish_stage'
  post 'games/:game_id/save_statistic_anwers', to: 'user_games#save_statistic_anwers', as: 'user_game/save_statistic_anwers'

  namespace :statistics do
    get 'best_display'
    get 'best_close'
    get 'worst_display'
    get 'worst_close'
    get 'game_display'
    get 'game_close'
    get 'post_display'
    get 'post_close'
    get 'count_display'
    get 'count_close'
  end

  get 'callbacks/facebook'
  get 'search', to: 'pages#search'
  get 'pages/:url', to: 'pages#show', as: 'page'

  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  devise_scope :user do
    get 'login', to: 'devise/sessions#new'
    authenticated :user do
      root to: 'admin/dashboard#index', as: :authenticated_root
    end
    unauthenticated :user do
      root to: 'users/sessions#new', as: :unauthenticated_root
    end
  end

end
