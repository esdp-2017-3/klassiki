#!/bin/bash

set -e

bundle
rake db:migrate RAILS_ENV=test
mailcatcher
bundle exec cucumber
