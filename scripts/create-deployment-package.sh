#!/bin/bash

set -e

build_number=${1}

RAILS_ENV=production rake assets:precompile

echo ${build_number} > VERSION

tar -czf ${HOME}/artifacts/klassiki-${build_number}.tar.gz \
--exclude=README.md \
--exclude=LICENSE \
--exclude=features \
--exclude=cucumber* \
--exclude=scripts \
--exclude=test \
--exclude=.idea \
--exclude=.git \
.