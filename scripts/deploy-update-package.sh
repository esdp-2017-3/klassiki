#!/bin/bash

set -e

host=${1}               # username@xx.xx.xx.xx
build_number=${2}

package_name=klassiki-${build_number}.tar.gz
package_name_path=${HOME}/artifacts/${package_name}

scp  ${package_name_path} "${host}:~"

number_build_demo=$(ssh ${host} cat rails-klassiki/VERSION)
ssh ${host} tar -czf backups/klassiki-${number_build_demo}.tar.gz rails-klassiki/*

ssh ${host} << HERE
#!/bin/bash

set -e
cd database_backups/
pg_dump klassiki_development > database_${number_build_demo}
HERE

ssh ${host} cp -rf rails-klassiki/public/* public_files/

ssh ${host} << HERE
#!/bin/bash

set -e

cd rails-klassiki/

rm Gemfile
rm Gemfile.lock
rm Rakefile
rm VERSION
rm -rf app
rm -rf bin
rm -rf config
rm -rf db
rm -rf lib
rm -rf log
rm -rf public
rm -rf script
rm -rf tmp
rm -rf vendor
HERE

ssh ${host} tar -xf klassiki-${build_number}.tar.gz -C rails-klassiki
ssh ${host} rm klassiki-${build_number}.tar.gz

ssh ${host} << HERE
#!/bin/bash

set -e

cd rails-klassiki/

rm -rf public/*
cd ..
cp -rf public_files/* rails-klassiki/public/
HERE

ssh ${host} rm -rf public_files/*

ssh ${host} rm rails-klassiki/config/database.yml
ssh ${host} cp files/database.yml rails-klassiki/config

ssh ${host} << HERE
#!/bin/bash

set -e

cd rails-klassiki/
bundle install
RAILS_ENV=production rake db:migrate
RAILS_ENV=production rake assets:precompile
HERE

ssh ${host} 'echo -e "\n" >> rails-klassiki/Gemfile'
ssh ${host} 'echo "gem \"unicorn\"" >> rails-klassiki/Gemfile'

ssh ${host} << HERE
#!/bin/bash

set -e

cd rails-klassiki/
bundle install
HERE

ssh ${host} cp files/unicorn.rb rails-klassiki/config

#ssh ${host} mkdir -p rails-klassiki/shared/pids rails-klassiki/shared/sockets rails-klassiki/shared/log