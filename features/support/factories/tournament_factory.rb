FactoryGirl.define do
  factory :tournament do
    title { Faker::HarryPotter.character }
    description { Faker::HarryPotter.quote }
    active { true }

    factory :inactive_tournament do
      active { false }
    end
  end
end