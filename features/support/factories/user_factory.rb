FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password { 'password' }
    password_confirmation { 'password' }
    role { 'user' }
    active { true }
    confirmed_at { DateTime.now }

    factory :admin do
      role { 'admin' }
      email { 'admin@example.com' }
    end

    factory :ban_user do
      role { 'user' }
      email { 'ban_user@example.com' }
      password { 'password' }
      password_confirmation { 'password' }
    end

    factory :test_user do
      email { 'test@example.com' }
    end

    factory :google_user do
      email { 'klassikiuser@gmail.com' }
    end

    factory :google_user2 do
      password { 'Klassiki22' }
    end

    factory :guest do
      email { "guest_#{(Random.rand(1..255)*52)}@example.com"}
      role { "none" }
    end
  end
end