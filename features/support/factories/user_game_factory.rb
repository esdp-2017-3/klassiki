FactoryGirl.define do
  factory :user_game do
    total_score        { 0 }
    total_time         { 0 }
    user_id            { 0 }
    game_id            { 0 }

    factory :rank_for_game do
      total_score        { 0 }
      total_time         { 0 }
      user_id            { 0 }
      game_id            { 0 }
    end

    factory :statistic_game do
      total_score { 12 }
    end
  end
end