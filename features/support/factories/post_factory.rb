FactoryGirl.define do
  factory :post do
    title { Faker::HarryPotter.character }
    body { Faker::HarryPotter.quote }
    images { nil }
    count { 0 }
    logo { nil }
    url { "https://www.facebook.com/ProektKlassiki/" }
  end
end