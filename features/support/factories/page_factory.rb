FactoryGirl.define do
  factory :page do
    title { Faker::HarryPotter.character }
    content { Faker::HarryPotter.quote }
    active { true }
    url { Faker::Color.color_name }
  end
end