FactoryGirl.define do
  factory :image do
    attachment { Rails.root.join('app', 'assets', 'images', 'test1.jpg').open }
    question_id { 1 }

    factory :demoimage do
      attachment { Rails.root.join('app/assets/images/game_images/game_1_8.jpg').open }
    end
  end
end