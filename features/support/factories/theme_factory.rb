FactoryGirl.define do
  factory :theme do
    name { Faker::HarryPotter.character }

    factory :literature do
      name { 'Литература' }
    end
  end
end