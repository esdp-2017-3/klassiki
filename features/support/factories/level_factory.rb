FactoryGirl.define do
  factory :level do
    name { Faker::HarryPotter.character }
    description { Faker::HarryPotter.quote }

    factory :beginner do
     name { 'Beginner' }
    end
  end
end