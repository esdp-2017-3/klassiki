FactoryGirl.define do
  factory :game_rating do
    user_id { 1 }
    game_id { 1 }
    rating { 8 }
  end
end