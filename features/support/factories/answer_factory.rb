FactoryGirl.define do
  factory :answer do
    body { Faker::HarryPotter.character }
    question_id { nil }
  end
end