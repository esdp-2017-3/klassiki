FactoryGirl.define do
  factory :game do
    title { Faker::HarryPotter.character }
    start_time { Faker::HarryPotter.quote }
    tournament_id { nil }
    level_id { 1 }
    theme factory: :theme
    paid { false }
    desc { Faker::Pokemon.name }
    game_image { nil }
    active { true }

    factory :demogame do
      title { 'Книжные Герои' }
      desc  { 'Здесь будет описание' }
      level { create(:beginner) }
      active { true }
      start_time { Time.current + 2.days }
      # game_image { Rails.root.join('app/assets/images/book_st.jpg').open }
    end

    factory :finished_game do
      title { 'Завершенная игра' }
      desc  { 'Игра уже сыграна' }
      level { create(:beginner) }
      active { true }
      start_time { Time.current - 1.days }
    end

    factory :inactive do
      active { false }
    end
  end
end