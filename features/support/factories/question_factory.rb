FactoryGirl.define do
  factory :question do
    body { Faker::Lorem.sentence }
    answer_desc { Faker::Lorem.sentence }
    format { Faker::Lorem.sentence }
    score { [1, 2, 3].sample }
    stage { [1, 2].sample }
    game_id { nil }
    about { Faker::Lorem.sentence }

    factory :demoquestion do
    	body { 'Демовопрос' }
    	game_id { 1 }
    	
        # images { Rails.root.join('app/assets/images/game_images/game_1_8.jpg').open }

    	# images do
    	# 	File.open(File.join(Rails.root, 'app/assets/images/game_images/game_1_8.jpg'))
    	# end

      # images { Rack::Test::UploadedFile.new(File.join(Rails.root, 'app/assets/images/game_images/game_1_8.jpg') }
    end
  end
end