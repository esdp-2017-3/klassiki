# language: ru
@javascript
Функционал: Администраторская панель
  Как администратор
  Администратор может войти в админку
  Чтобы управлять тематиками

  Сценарий: Создание тематики
    Допустим администратор сайта залогинился с данными:
      | email             | password   |
      | admin@example.com | password |
    Если он перейдет в админ-панель
    И перейдет к странице добавления тематики
    И заполнит форму для создания тематики:
      | name       |
      | test theme |
    То тематика создастся

  Сценарий: Удаление тематики
    Допустим администратор сайта залогинился с данными:
      | email             | password |
      | admin@example.com | password |
    Если он перейдет в админ-панель
    И перейдет к странице тематики
    И удалит тематику из списка
    То тематика будет удалена

  Сценарий: Изменение тематики
    Допустим администратор сайта залогинился с данными:
      | email             | password |
      | admin@example.com | password |
    Если он перейдет в админ-панель
    И перейдет к странице тематики
    И отредактирует тематику из списка данными:
      | name              |
      | test theme edited |
    То тематика будет отредактирована