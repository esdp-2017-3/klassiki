Допустим(/^пользователь зашел на главную страницу$/) do
  visit root_path
end

Если(/^он видит виджет Facebook$/) do
  find_by_id('facebook_widget')
end

То(/^в нем должна быть ссылка на страницу группы$/) do
  within_frame('facebook_widget') do
    find_link(href: 'https://www.facebook.com/ProektKlassiki/', title: 'Интеллектуальный проект "Классики"')
  end
end