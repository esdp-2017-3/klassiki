When(/^пользователь залогинился на сайт с данными:$/) do |table|
  create(:user, email: 'user@example.com', alias: 'User')

  visit '/users/sign_in'
  within("#new_user") do
    fill_in 'Email', with: table.hashes[0][:email]
    fill_in 'Пароль', with: table.hashes[0][:password]
  end
  click_button 'Войти'
  sleep 1
end

When(/^перешел на страницу редактирования профиля$/) do
  find('a.dropdown-toggle').click
  click_on 'Изменить данные'
  sleep 1
end

When(/^изменил данные:$/) do |table|
  sleep 1
  within("#edit_user") do
    fill_in 'Псевдоним', with: table.hashes[0][:name]
    fill_in 'Текущий пароль', with: table.hashes[0][:current_password]
  end
  click_button 'Update'
  sleep 1
end

When(/^данные должны быть обновлены$/) do
  assert_text('Ваша учетная запись изменена')
end

When(/^перейдет к странице добавления пользователя$/) do
  create(:user, alias: 'User')
  create(:user, alias: 'User delete', active: false)
  create(:ban_user, alias: 'Ban')

  click_link 'Пользователь'
end

When(/^удалит пользователя c id "([^"]*)"$/) do |id|
  within("#user_#{id}") do
    click_link 'Удалить'
  end
end

When(/^пользователь "([^"]*)" будет удалён$/) do |user|
  page.assert_text("#{user} удалён")
end

When(/^восстановит пользователя c id "([^"]*)"$/) do |id|
  within("#user_#{id}") do
    click_link 'Восстановить'
  end
end

When(/^пользователь "([^"]*)" будет восстановлен$/) do |user|
  page.assert_text("#{user} восстановлен")
end

When(/^создаст пользователя$/) do
  create(:user, alias: 'Ban user', active: false)
end

When(/^изменит пользователя c id "([^"]*)"$/) do |id|
  sleep 5
  within("#user_#{id}") do
    click_link 'Изменить'
  end
end


When(/^заблокирует пользователя$/) do

  select("месяц")
  click_on 'Обновить элемент'
  sleep 5
end

When(/^админ покинет сайт$/) do
  visit('/')
  click_on('Аккаунт')
  click_on('Выйти')
end

