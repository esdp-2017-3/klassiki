When(/^перешел в личный кабинет$/) do
  @game = create(:demogame, id: 1 )
  @user_game = create( :user_game, game_id: 1, total_time: 20, total_score: 3, user_id: 1)
  find('a.dropdown-toggle').click
  click_on 'Личный кабинет'
  sleep 5
end

When(/^его alias видно в личном кабинете$/) do
  page.assert_text('User')
  sleep 5
end

When(/^сыгранная игроком игра отображается в личном кабинете$/) do
  assert_text 'Книжные Герои'
  sleep 5
end

When(/^пользователь перейдет на страницу профиля$/) do
  find('a.dropdown-toggle').click
  click_on 'Личный кабинет'
  sleep 5
end

When(/^сможет загрузить себе аватар$/) do
  find(:xpath, '/html/body/div/div/div[1]/div[2]/a')
  create(:admin, id:2)
end

When(/^если зайдет в профиль другого пользователя$/) do
  visit ('/profiles/2')
  sleep 5
end

When(/^не сможет изменить его аватар$/) do
  assert_no_text "Изменить фото"
  sleep 5
end

