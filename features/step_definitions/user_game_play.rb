include CapybaraHelper

When(/^пользователь перейдет на страницу списка игр$/) do
  level = create(:level, name: 'test level')
  game = create(:game, title: 'test game', desc: 'test description', level_id: level.id)
  question1 = create(:question, stage: 1, game_id: game.id, answer_desc: 'Вопрос про прыгательный аттракцион')
  game = create(:game, title: 'название игры', desc: 'test description', level_id: level.id)

  visit(root_path)
  click_on 'test level'
end

When(/^нажмет на название игры "([^"]*)"$/) do |game_title|
  sleep 3
  click_on "#{game_title}"
end

When(/^увидит название игры "([^"]*)" и надпись "([^"]*)"$/) do |game_name, text|
  page.assert_text("#{game_name}")
  page.assert_text("#{text}")
end

When(/^нажмет на ссылку "([^"]*)"$/) do |link|
  click_on "#{link}"
end

When(/^увидит результат результат и ссылку "([^"]*)"$/) do |link|
  page.assert_text("Результат")
  page.assert_text("#{link}")
end

When(/^увидит результат, но не увидит ссылку "([^"]*)"$/) do |link|
  page.assert_text("Результат")
  page.assert_no_text("#{link}")
end

When(/^форма ответа на этот вопрос не будет видна пользователю$/) do
  assert(page.has_no_xpath?("//*[contains(text(), 'Ответить')]"))
end

When(/^заполнит форму ответов для статистики$/) do
  within('#form_for_statistic') do
    select(8, from: 'rating')
    choose('Отметьте понравившийся вопрос', match: :first)
    choose('Отметьте тяжелый вопрос', match: :first)
  end
  click_button 'Покинуть игру'
end

When(/^увидит "([^"]*)"$/) do |arg|
  page.assert_text("#{arg}")
end


When(/^не увидит результаты первого этапа и "([^"]*)"$/) do |question_about|
  page.assert_no_text("#{question_about}")
end

When(/^увидит результат игры и "([^"]*)"$/) do |question_about|
  page.assert_text("#{question_about}")
end

When(/^я увижу описание данного турнира$/) do
  page.assert_text('Beginner - игры, сложность которых является минимальной. Задания в играх рассчитаны на игроков,которые только знакомятся с особенностью вопросов игры.')
end

When(/^пользователь перейдет на страницу списка игр уровня "([^"]*)"$/) do |begginer|
  LevelsHelper.create_levels

  visit(root_path)
  visit('/levels/3')
end

When(/^вижу описание игры и кнопку "([^"]*)"$/) do |button|
  sleep 2
  page.assert_text("#{button}")
  page.assert_text('test description')
end

When(/^увидит кнопку "([^"]*)"$/) do |button|
  page.assert_text("#{button}")
end

When(/^после истечения времени произойдет нажатие на ссылку "([^"]*)"$/) do |arg|
  sleep 5
end

When(/^увидит результат таймер$/) do
  page.assert_text('Time')
end

When(/^подтвердит действие$/) do
  page.accept_confirm
end

When(/^будет существовать игра другого польователя$/) do
  game = create(:game, title: 'game', desc: 'description')
  question1 = create(:question, stage: 1, game: game, answer_desc: 'Вопрос ')
  user = create(:user)
  user_game = create(:user_game, game: game, user: user)
  sleep 2
end

When(/^пользователь перейдет на страницу игры другого пользователя$/) do
  visit '/games/2/play/'
end

When(/^существует игра с ответом "([^"]*)"$/) do |answer|
  game = create(:game, title: 'game', desc: 'description')
  question = create(:question, stage: 1, game: game, answer_desc: 'Вопрос ')
  answer = create(:answer, body: answer, question: question)
end

When(/^ответит на вопрос "([^"]*)"$/) do |answer|
  click_on 'Вопрос #1'
  within('#new_user_answer') do
    fill_in('user_answer_body', with: answer)
  end
  click_button 'Ответить'
end

When(/^ответ будет верным$/) do
  find('div.panel-success')
end