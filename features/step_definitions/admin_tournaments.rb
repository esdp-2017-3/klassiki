include CapybaraHelper

#Создание турнира
When(/^администратор сайта залогинился с данными:$/) do |table|
  user = create(:user, email: 'admin@example.com', role: 'admin', alias: 'Admin')
  visit '/users/sign_in'
  within("#new_user") do
  fill_in 'Email', with: table.hashes[0][:email]
  fill_in 'Пароль', with: table.hashes[0][:password]
  end
  click_button 'Войти'
end

When(/^он перейдет в админ\-панель$/) do
  visit '/admin'
  sleep 1
  page.assert_selector(:xpath, '//*[@id="current_user"]/a')
  page.assert_text('admin@example.com')
end

When(/^перейдет к странице добавления турнира$/) do
  click_link 'Турнир'
  click_link 'Создать Турнир'
end

When(/^заполнит форму:$/) do |table|
  image = table.hashes[0][:tournament_image]
  image = Rails.root.join('app/assets/images', image)
  within('#new_tournament') do
    attach_file('Логотип турнира', image)
  end
  within("#new_tournament") do
    fill_in 'tournament_title', with: table.hashes[0][:title]
    # fill_in 'tournament_description', with: table.hashes[0][:description]
  end
  tinymce_fill_in('tournament_description', options = {with: table.hashes[0][:description]})
  sleep 1
  click_button 'Создать элемент'
  sleep 1
end

When(/^турнир создастся$/) do
  find('td', text: 'Тестовый турнир')
end

Если(/^перейдет к странице турниров$/) do
  create(:tournament)

  click_link 'Турнир'
  page.find('a', text: '1')
  sleep 1
end

Если(/^удалит турнир из списка$/) do
  page.assert_selector('#index_table_tournaments')
  find(:xpath, '//*[@id="tournament_1"]/td[10]/div/a[3]').click
  page.driver.browser.switch_to.alert.accept #Confirm?
  sleep 1
end

То(/^турнир будет удален$/) do
  page.assert_no_selector('a', text: '1')
end

When(/^отредактирует турнир из списка$/) do
  page.assert_selector('#index_table_tournaments')
  find(:xpath, '//*[@id="tournament_1"]/td[10]/div/a[2]').click
  sleep 1
  within("#edit_tournament") do
    fill_in 'tournament_title', with: 'Lorem Ipsum Title'
  end
  tinymce_fill_in('tournament_description', options = {with: 'Lorem Ipsum Description'})
  click_button 'Обновить элемент'
end

When(/^турнир будет отредактирован$/) do
  page.find('td', text: 'Lorem Ipsum Title')
end