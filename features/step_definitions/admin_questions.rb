include CapybaraHelper

When(/^заполнит форму добавления вопроса данными:$/) do |table|
  # table is a table.hashes.keys # => [:game, :body, :answer_desc, :format, :about]
  create(:game, title: 'Test Game')

  visit('/admin/questions/new')
  within('#new_question') do
    select(table.hashes[0][:game], from: 'question_game_id')
    choose 'question_stage_1'
    choose 'question_score_2'
    fill_in('question_about', with: table.hashes[0][:about])
    fill_in('question_answer_desc', with: table.hashes[0][:answer_desc])
    fill_in('question_format', with: table.hashes[0][:format])
  end
  tinymce_fill_in('question_body', options = {with: table.hashes[0][:body]})
end

When(/^нажмет на кнопку для добавления картинки$/) do
  click_on 'Добавить Изображение'
end

When(/^добавит картинку:$/) do |table|
  # table is a table.hashes.keys # => [:attachment]
  within('#question_images_attributes_0_attachment_input') do
    attach_file('Attachment', Rails.root.join('app/assets/images/sampleimages', table.hashes[0][:attachment]))
  end
end

When(/^нажмет на кнопку для добавления аудиофайла$/) do
  click_on 'Добавить аудиофайл'
end

When(/^добавит аудиофайл$/) do |table|
  # table is a table.hashes.keys # => [:attachment]
  within('#question_audios_attributes_0_attachment_input') do
    attach_file('Attachment', Rails.root.join('app/assets/images/samplemusic', table.hashes[0][:attachment]))
  end
end

When(/^нажмет на кнопку для добавления ответа$/) do
  click_on 'Добавить Ответ'
end

When(/^добавит ответ$/) do |table|
  within('#question_answers_attributes_0_body_input') do
    fill_in('question_answers_attributes_0_body', with: table.hashes[0][:body])
  end
end

When(/^нажмет на кнопку для добавления вопроса$/) do
  click_button 'Создать элемент'
end

When(/^название "([^"]*)" видно$/) do |body|
  find(:xpath, "//*[contains(text(), '#{body}')]")
end


When(/^нажимает "([^"]*)" для вопроса "([^"]*)"$/) do |button, question|
  create(:question, body: 'Question to test')
  create(:game, title: 'Test Game')

  visit('/admin/questions')
  element = find(:xpath, "//*[contains(text(), '#{question}')]/../..//a[contains(text(), '#{button}')]")
  element.click
end

When(/^заполнит форму изменения вопроса данными:$/) do |table|
  # table is a table.hashes.keys # => [:game, :body, :answer_desc, :format, :about]
  within('#edit_question') do
    select(table.hashes[0][:game], from: 'question_game_id')
    choose 'question_stage_2'
    choose 'question_score_3'
    fill_in('question_about', with: table.hashes[0][:about])
    fill_in('question_answer_desc', with: table.hashes[0][:answer_desc])
    fill_in('question_format', with: table.hashes[0][:format])
  end
  tinymce_fill_in('question_body', options = {with: table.hashes[0][:body]})
end

When(/^нажмет на кнопку для изменения вопроса$/) do
  click_button 'Обновить элемент'
end

When(/^подтверждает удаление$/) do
  accept_confirm
end

When(/^название "([^"]*)" не видно в списке вопросов$/) do |question|
  sleep 5
  visit('/admin/questions')
  assert(page.has_no_xpath?("//table[@id='index_table_questions']//a[contains(text(), '#{question}')]"))
end