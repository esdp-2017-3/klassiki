When(/^есть турнир$/) do
  @tournament = create(:tournament, title: 'Test tournament')
  @game = create(:demogame, tournament_id: @tournament.id)
end

When(/^есть неактивный турнир$/) do
  sleep 1
  @inactive = create(:inactive_tournament, title: "Неактивный турнир")
  @game_for_inactive = create(:demogame, tournament_id: @inactive.id)
end

When(/^увидит название турнира и кнопку статистика турнира$/) do
  assert_text('Test tournament')
  assert_text('Статистика турнира')
end

When(/^есть активный и неактивный турниры$/) do
  @inactive = create(:inactive_tournament, title: "Неактивный турнир")
  @active = create(:tournament, title: "Активный турнир")
  @game_for_inactive = create(:demogame, tournament_id: @inactive.id)
  @game_for_active   = create(:demogame, tournament_id: @active.id)
end

When(/^он не видит неактивный турнир$/) do
  @inactive = create(:inactive_tournament, title: "Неактивный турнир")
  @active = create(:tournament, title: "Активный турнир")
  @game_for_inactive = create(:demogame, tournament_id: @inactive.id)
  @game_for_active   = create(:demogame, tournament_id: @active.id)

  visit root_path
  assert_no_text "Неактивный турнир"
end

When(/^видит активный турнир$/) do
  assert_text "Активный турнир"
end

Если(/^он переходит на страницу неактивного турнира$/) do
  sleep 1
  visit ('/tournaments/2')
end

When(/^увидит сообщение "([^"]*)"$/) do |arg|
  assert_text arg
end

#Ближайшие игры
When(/^есть активный турнир$/) do
  @tournament = create(:tournament, title: "Активный турнир")
end

When(/^у него есть игра$/) do
  @game = create(:demogame, tournament: @tournament, title: 'Актуальная игра')
end

When(/^пользователь зашел на страницу турнира$/) do
  visit root_path
  click_link 'Узнать больше'
end

When(/^он видит актуальную игру$/) do
  assert_text 'Актуальная игра'
end

When(/^видит счетчик$/) do
  assert_text 'Начнется через'
end


When(/^у него есть игра с результатами$/) do
  @game = create(:finished_game, tournament_id: @tournament.id)
  @user = create(:user, alias: 'Игрок')
  create(:statistic_game, game_id: @game.id, user_id: @user.id)
end

When(/^пользователь зашел на страницу статистику турнира$/) do
  visit root_path
  click_link 'Узнать больше'
  click_link 'Статистика турнира'
end

When(/^на странице будут отображены результаты$/) do
  assert_text 'Общие результат'
  assert_text 'Завершенная игра'
  assert_text '12'
end