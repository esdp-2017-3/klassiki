То(/^увидит ее содержимое$/) do
  page.assert_text("https://www.facebook.com/ProektKlassiki/")
end

Если(/^нажмет "([^"]*)"$/) do |link|
  click_link link
  sleep 1
end

То(/^вернется обратно на главную страницу$/) do
  find('div', class: 'news')
end
