Если(/^перейдет к странице вопросов$/) do
  @url = current_url

  question = create(:question, body: 'Question for destroy')
  create(:image, question_id: question.id)

  visit 'admin/questions'
  find_by_id('index_table_questions')
end

Если(/^удалит вопрос из списка$/) do
  assert_equal true, File.exist?('public/uploads/image/attachment/1/test1.jpg')
  sleep 10
  find('tr', text: 'Question for destroy').find('a', text: 'Удалить').click
  page.driver.browser.switch_to.alert.accept

end

То(/^картинка из папки тоже будет удалена$/) do
  sleep 1
  assert_equal false, File.exist?('public/uploads/image/attachment/1/test1.jpg')
end