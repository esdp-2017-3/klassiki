When(/^перейдет к странице добавления тематики$/) do
  click_link 'Тематика'
  click_link 'Создать Тематику'
  sleep 1
end

When(/^заполнит форму для создания тематики:$/) do |table|
  within("#new_theme") do
    fill_in 'theme_name', with: table.hashes[0][:name]
  end
  click_button 'Создать элемент'
  sleep 2
end

When(/^тематика создастся$/) do
  sleep 1
  page.assert_text('test theme')
end

When(/^перейдет к странице тематики$/) do
  create(:theme, name: 'Theme to delete')

  click_link 'Тематика'
  sleep 1
  page.assert_selector('#index_table_themes')
  sleep 1
end

When(/^удалит тематику из списка$/) do
  within('#theme_1') do
    assert_text('Theme to delete')
    click_link 'Удалить'
  end
  page.accept_confirm
  sleep 1
end

When(/^тематика будет удалена$/) do
  page.assert_text('Элемент успешно удалён.')
  page.assert_no_text('Theme to delete')
  sleep 2
end

When(/^отредактирует тематику из списка данными:$/) do |table|
  create(:theme, name: 'Theme to edit')

  sleep 1
  page.assert_selector('#index_table_themes')
  sleep 1
  within("#theme_1") do
    click_link 'Изменить'
  end
  sleep 1
  within("#edit_theme") do
    fill_in 'theme_name', with: 'Edited Theme'
  end
  sleep 1
  click_button 'Обновить элемент'
  sleep 1
end

When(/^тематика будет отредактирована$/) do
  page.assert_text('Edited Theme')
end
