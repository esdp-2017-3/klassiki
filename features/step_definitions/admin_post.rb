include CapybaraHelper

When(/^перейдет к странице добавления новостей$/) do
  click_link 'Новость'
  click_link 'Создать Новость'
end
When(/^заполнит форму добавления новостей, добавив картинку:$/) do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  images = table.hashes[0][:images].split(',')
  images = images.map do |image_name|
    Rails.root.join('app/assets/images/postimages', image_name)
  end
  within('#new_post') do
    attach_file('Картинки', images)
  end
  within("#new_post") do
    fill_in 'post_title', with: table.hashes[0][:title]
  end
  tinymce_fill_in('post_body', options = {with: table.hashes[0][:body]})
  click_button 'Создать элемент'
end
When(/^новость создастся$/) do
  page.assert_text('Элемент успешно создан.')
end

When(/^перейдет к странице новостей$/) do
  create(:post)

  click_link 'Новость'
  sleep 1
end
When(/^отредактирует новость из списка$/) do
  page.assert_selector('#index_table_posts')
  within("#post_1") do
    click_link 'Изменить'
  end
  sleep 1

  within("#edit_post") do
    fill_in 'post_title', with: 'first'
  end
  tinymce_fill_in('post_body', options = {with: "some text"})

  click_button 'Обновить элемент'
end
When(/^новость будет отредактирована$/) do
  page.assert_text('Элемент успешно обновлен.')
end

When(/^удалит новость из списка$/) do
  page.assert_selector('#index_table_posts')
  within("#post_1") do
    click_link 'Удалить'
  end
  page.driver.browser.switch_to.alert.accept
end
When(/^новость будет удалена$/) do
  page.assert_text('Элемент успешно удалён.')
end


When(/^пользователь заходит на главную станицу$/) do
  visit('/')
end
When(/^он должен увидеть новости$/) do
  page.assert_text('Новости')
end
