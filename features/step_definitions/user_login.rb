When(/^пользователь зашел на сайт:$/) do
  create(:user, email: 'user@example.com')

  visit root_path
end

When(/^перешел к форме входа$/) do
  sleep 1
  click_on('Войти')
  click_on('Обычный вход')
end

When(/^ввел данные и кликнул кнопку "([^"]*)"$/) do |button, table|
  within("#new_user") do
    fill_in 'Email', with: table.hashes[0][:email]
    fill_in 'Пароль', with: table.hashes[0][:password]
  end
  click_button button
  sleep 1
end

When(/^пользователь должен быть залогинен$/) do
  page.assert_text('Аккаунт')
end

When(/^выбирает вход через Google в навигации$/) do
  click_on('Войти')
  click_on('Google')
  sleep 5
end

When(/^он введет свою почту$/) do
  sleep 3
  fill_in 'identifierId', with: 'klassikitest@gmail.com'
  sleep 5
end

When(/^подтвердит свои действия$/) do
  sleep 3
  find(:id, 'identifierId').native.send_keys(:enter)
  sleep 5
end

Если(/^введет свой пароль$/) do
  find(:xpath, "//*[@id='password']/div[1]/div/div[1]/input").set "Klassiki22"
  find(:xpath, "//*[@id='password']/div[1]/div/div[1]/input").native.send_keys(:enter)

  sleep 5
end


When(/^будет выполнен вход на наш сайт через Google$/) do
   assert_text "Аккаунт"
  sleep 5
end

