include LevelsHelper

When(/^он видит новости$/) do
  create(:post, title: 'test_news')
  create_list(:post, 10)
  create(:post, title: 'test_news_111')

  visit('/')
  page.assert_text('test_news_111')
end


When(/^пользователь нажмет кнопку "([^"]*)"$/) do |button|
  click_link(button)
  sleep(2)
end


When(/^перейдет на вторую страницу и увидит следующие новости$/) do
  page.assert_text('test_news')
end


When(/^перейдет на главную страницу и увидит первые новости$/) do
  page.assert_text('test_news_111')
end

When(/^пользователь зашел на страницу игр$/) do
  LevelsHelper.create_levels
  create(:game, title: 'Aagame', level_id: 1)
  create_list(:game, 10)
  create(:game, title: 'Zzgame', level_id: 1)

  visit('/levels/1')
end

When(/^он видит игры$/) do
  page.assert_text('New Wave')
  sleep(2)
end

When(/^перейдет на вторую страницу и увидит следующие игры$/) do
  sleep 3
  page.assert_text('Zzgame')
  sleep(2)
end

When(/^перейдет на предыдущую страницу и увидит первые игры$/) do
  page.assert_text('Aagame')
end

