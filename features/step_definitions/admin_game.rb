include CapybaraHelper

When(/^перейдет к странице добавления игры$/) do
  create_list(:tournament, 5)
  create(:theme)
  CapybaraHelper.create_levels
  create(:question, stage: 1)
  create(:question, stage: 2)

  click_link 'Игра'
  click_link 'Создать Игру'
  sleep 1
end

When(/^заполнит форму добавления игры:$/) do |table|
  page.assert_selector(:xpath, '//*[@id="new_game"]')
  find('#game_tournament_id').find(:xpath, 'option[2]').select_option
  find('#game_level_id').find(:xpath, 'option[2]').select_option
  find('#game_theme_id').find(:xpath, "option[#{table.hashes[0][:theme]}]").select_option
  image = table.hashes[0][:game_image]
  image = Rails.root.join('app/assets/images', image)
  within('#new_game') do
    attach_file('Логотип игры', image)
  end
  within("#new_game") do
    fill_in 'game_title', with: table.hashes[0][:title]
  end
  tinymce_fill_in('game_desc', options = {with: table.hashes[0][:desc]})
  find(:css, "#game_question_ids_1").set(true)
  find(:css, "#game_question_ids_2").set(true)
  click_button 'Создать элемент'
  sleep 2
end

When(/^игра создастся$/) do
  sleep 2
  find('td', text: 'Тестовая игра')
  sleep 1
  find('td', text: 'New Wave')
  sleep 1
end

When(/^перейдет к странице игр$/) do
  create(:game, title: 'Игра для удаления')

  click_link 'Игра'
  sleep 1
  page.assert_selector('#index_table_games')
  sleep 1
end

When(/^удалит игру из списка$/) do
  within('#game_1') do
    assert_text('Игра для удаления')
    click_link 'Удалить'
  end
  page.driver.browser.switch_to.alert.accept
  sleep 1
end

When(/^игра будет удалена$/) do
  page.assert_text('Элемент успешно удалён.')
  page.assert_no_text('Игра для удаления')
  sleep 3
end

When(/^отредактирует игру из списка$/) do
  create(:game, title: 'Игра для удаления')
  create(:question, stage: 1)
  create(:question, stage: 2)
  create(:question, stage: 1)
  create(:question, stage: 2)
  create_list(:tournament, 5)

  sleep 1
  page.assert_selector('#index_table_games')
  sleep 1
  within("#game_1") do
    click_link 'Изменить'
  end
  sleep 1
  find('#game_tournament_id').find(:xpath, 'option[2]').select_option
  within("#edit_game") do
    fill_in 'game_title', with: 'Lorem Ipsum Game'
    find(:css, "#game_question_ids_3").set(true)
    find(:css, "#game_question_ids_4").set(true)
  end
  find(:xpath, '//*[@id="game_start_time_1i"]').find(:xpath, 'option[7]').select_option
  find(:xpath, '//*[@id="game_start_time_2i"]').find(:xpath, 'option[5]').select_option
  find(:xpath, '//*[@id="game_start_time_3i"]').find(:xpath, 'option[13]').select_option
  find(:xpath, '//*[@id="game_start_time_4i"]').find(:xpath, 'option[2]').select_option
  find(:xpath, '//*[@id="game_start_time_5i"]').find(:xpath, 'option[2]').select_option
  sleep 1
  click_button 'Обновить элемент'
  sleep 1
end

When(/^игра будет отредактирована$/) do
  sleep 2
  find('tr', class: 'row-title').find('td', text: 'Lorem Ipsum Game')
end