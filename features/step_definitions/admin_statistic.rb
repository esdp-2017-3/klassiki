When(/^он видит названия колонок статистики$/) do
  game = create(:game)
  question1 = create(:question, body: 'В основе названия некого прыгательного аттракциона лежит имя именно этого героя')
  question2 = create(:question, body: 'Кыргызстанский город, в котором последние три буквы заменены витамином аскорбиновой кислоты - это известный персонаж. Какой?')
  create(:game_rating, game_id: game.id)
  create(:question_best, question_id: question1.id)
  create(:question_best, question_id: question2.id)
  create(:question_worst, question_id: question1.id)
  create(:question_worst, question_id: question2.id)
  create(:post, title: 'test_news_111 (5)', count: 5)

  visit '/admin'
  page.assert_text('Игра (Сколько раз была сыграна)')
  page.assert_text('Новости (Количество просмотренных)')
  page.assert_text('Рейтинг игр')
  page.assert_text('Лучшие вопросы:')
  page.assert_text('Тяжелые вопросы:')
  page.assert_text('test_news_111 (5)')
  page.assert_text('В основе названия некого прыгательного аттракциона лежит имя именно этого героя (1)')
  page.assert_text('Кыргызстанский город, в котором последние три буквы заменены витамином аскорбиновой кислоты - это известный персонаж. Какой? (1)')
end