When(/^посетитель переходит на страницу регистрации$/) do
  click_link 'Регистрация'
  sleep 1
end

When(/^видит форму регистрации$/) do
  page.assert_selector(:xpath, '//*[@id="new_user"]')
  page.assert_text('Создайте аккаунт для игры')
end

When(/^посетитель заполнит форму регистрации с данными:$/) do |table|
  within("#new_user") do
    fill_in 'Псевдоним', with: table.hashes[0][:name]
    fill_in 'Email', with: table.hashes[0][:email]
    fill_in 'Пароль', with: table.hashes[0][:password]
    fill_in 'Повторите пароль', with: table.hashes[0][:password_confirmation]
  end
  click_button 'Зарегистрироваться'
  sleep 1
end

When(/^пользователь будет создан$/) do
  page.assert_text('Письмо со ссылкой для подтверждения было отправлено на ваш адрес эл. почты. Пожалуйста, перейдите по ссылке, чтобы подтвердить учетную запись.')
  sleep 1
end

When(/^не увидит "([^"]*)"$/) do |text|
  page.assert_no_text("#{text}")
end