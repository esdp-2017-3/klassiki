When(/^есть контетная страница$/) do
  create(:page, title: "Тестовая страница", url: "test")
end

When(/^видит меню контетных страниц$/) do
  find('a', class: "contentpage")
end

When(/^попадет по маршруту "([^"]*)"$/) do |path|
  assert_equal true, current_path == path
end

When(/^на странице будет заголовок "([^"]*)"$/) do |title|
  find("h1", text: title)
end

When(/^на странице будет контент$/) do
  find("div", class: "content")
end