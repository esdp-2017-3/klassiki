When(/^он кликнет на ссылку "([^"]*)"$/) do |link|
  click_link link
end

When(/^попадет на страницу со списком игр по алфавиту$/) do
  assert_equal true, current_path == '/games'
  find_by_id 'games'
end

When(/^увидит количество игрового времени\("([^"]*)" минут\)$/) do |minutes|
  assert_text("Время на игру: #{minutes} мин.")
end