When(/^в приложении будет несколько неактивных игр$/) do
  10.times do
    create(:inactive, title: "Неактивная игра")
  end
end

When(/^я зайду на главную страницу$/) do
  visit root_path
end

When(/^они не будут отображены на главной странице$/) do
  assert_no_text('Неактивная игра')
end

When(/^я зайду на страницу отображения всех игр$/) do
  visit games_path
end

When(/^они не будут отображены на странице всех игр$/) do
  assert_no_text('Неактивная игра')
end