When(/^пользователь переходит на страницу статистики демо\-игры$/) do
  @demo_game = create(:demogame, id: 1)
  @user_game1 = create(:rank_for_game, id: 1, user_id: 1, game: @demo_game, total_score: 4, stage2_finish_time: "2017-05-26 07:55:25")
  @user_game2 = create(:rank_for_game, id: 2, user_id: 1, game: @demo_game, total_score: 6, stage2_finish_time: "2017-05-26 08:20:25")
  visit 'games/1/rank'
end

When(/^видит первый результат игрока$/) do
  assert_text 'Книжные Герои'
  find_by_id 'ranks'
  assert_text '4'
end

When(/^не видит второй результат игрока$/) do
  assert_no_text '6'
end

#Незаконченные игры
When(/^есть тестовый пользователь$/) do
  @user = create(:test_user)
end
When(/^есть демоигра$/) do
  @game = create(:demogame)
end

When(/^пользователь сыграл в первый тур игры$/) do
  create(:user_game, user: @user, game: @game, stage1_start_time: "2017-05-26 07:40:25", total_score: 3)
end

When(/^перейти в статистику$/) do
  visit '/games/1/rank'
end

When(/^этой игры там не будет$/) do
  assert_no_text 'Книжные Герои'
end

#Гостевая игра
When(/^есть игра$/) do
  @test_game = create(:demogame)
  @guest = create(:guest, alias: "guest")
end

When(/^гость сыграет в игру$/) do
  @user_game = create(:user_game, user: @guest, game: @test_game, total_score: 4, stage2_finish_time: "2017-05-26 07:55:25")
end