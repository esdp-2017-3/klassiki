include LevelsHelper

# Логотип
Допустим(/^пользователь зашел на сайт и видит лого$/) do
  visit root_path
  find_by_id('logo')
end

Если(/^он перейдет на любую страницу$/) do
  click_link 'Регистрация'
  sleep 1
end

То(/^логотип также должен быть отображен в навигации$/) do
  find_by_id('logo')
end

# Панель игр
Допустим(/^видит боковую панель с рандомными и лучшими играми$/) do
  LevelsHelper.create_levels

  game1  = create(:game, game_image: Rails.root.join("app/assets/images/game_images/1.jpg").open,  title: 'Beginner 1',    desc: 'Описание 1',  level_id: 1, paid: true, active: true)
  game2  = create(:game, game_image: Rails.root.join("app/assets/images/game_images/2.jpg").open,  title: 'Beginner 2',    desc: 'Описание 2',  level_id: 1, paid: true, active: true)
  game3  = create(:game, game_image: Rails.root.join("app/assets/images/game_images/3.jpg").open,  title: 'Beginner 3',    desc: 'Описание 3',  level_id: 1, paid: true, active: true)
  game4  = create(:game, game_image: Rails.root.join("app/assets/images/game_images/4.jpg").open,  title: 'New Wave 4',    desc: 'Описание 4',  level_id: 2, paid: true, active: true)
  game5  = create(:game, game_image: Rails.root.join("app/assets/images/game_images/5.jpg").open,  title: 'New Wave 5',    desc: 'Описание 5',  level_id: 2, paid: true, active: true)
  game6  = create(:game, game_image: Rails.root.join("app/assets/images/game_images/6.jpg").open,  title: 'New Wave 6',    desc: 'Описание 6',  level_id: 2, paid: true, active: true)
  game7  = create(:game, game_image: Rails.root.join("app/assets/images/game_images/7.jpg").open,  title: 'Old School 7',  desc: 'Описание 7',  level_id: 3, paid: true, active: true)
  game8  = create(:game, game_image: Rails.root.join("app/assets/images/game_images/8.jpg").open,  title: 'Old School 8',  desc: 'Описание 8',  level_id: 3, paid: true, active: true)
  game9  = create(:game, game_image: Rails.root.join("app/assets/images/game_images/9.jpg").open,  title: 'Old School 9',  desc: 'Описание 9',  level_id: 3, paid: true, active: true)
  game10 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/10.jpg").open, title: 'Детские 10',    desc: 'Описание 10', level_id: 4, paid: true, active: true)
  game11 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Детские 11',    desc: 'Описание 11', level_id: 4, paid: true, active: true)
  game12 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Детские 12',    desc: 'Описание 12', level_id: 4, paid: true, active: true)
  game13 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Корпорат 13',   desc: 'Описание 13', level_id: 5, paid: true, active: true)
  game14 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Корпорат 14',   desc: 'Описание 14', level_id: 5, paid: true, active: true)
  game15 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Корпорат 15',   desc: 'Описание 15', level_id: 5, paid: true, active: true)

  game16 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Беспл 16 Bg', desc: 'Описание 16', level_id: 1, paid: false, active: true)
  game17 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Беспл 17 NW', desc: 'Описание 17', level_id: 2, paid: false, active: true)
  game18 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Беспл 18 OS', desc: 'Описание 17', level_id: 3, paid: false, active: true)

  game19 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Лучш 19', desc: 'Описание 19', paid: nil, active: true)
  game20 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Лучш 20', desc: 'Описание 20', paid: nil, active: true)
  game21 = create(:game, game_image: Rails.root.join("app/assets/images/game_images/is.jpg").open, title: 'Лучш 21', desc: 'Описание 21', paid: nil, active: true)

  gamerating1 = create(:game_rating, game: game19, rating: 8)
  gamerating2 = create(:game_rating, game: game19, rating: 9)
  gamerating3 = create(:game_rating, game: game19, rating: 10)
  gamerating4 = create(:game_rating, game: game20, rating: 10)
  gamerating5 = create(:game_rating, game: game21, rating: 9)
  gamerating6 = create(:game_rating, game: game21, rating: 10)
  gamerating7 = create(:game_rating, game_id: 1,   rating: 10)

  visit('/')
  find(:xpath, '//*[@id="level"]/a[1]').assert_text("New Wave")
  find(:xpath, '//*[@id="level"]/a[5]').assert_text("Old School")
  find(:xpath, '//*[@id="level"]/a[9]').assert_text("Beginner")
  find(:xpath, '//*[@id="level"]/a[12]') #12 рандомных игр
  find(:xpath, '//*[@id="level-type"]/a[1]').assert_text("Детские:")
  find(:xpath, '//*[@id="level-type"]/a[5]').assert_text("Корпоративные:")
  find_by_id 'bestgame-3'
  find_by_id 'freegame-3'  
end

Если(/^он кликнет на одну из игр$/) do
  click_link('freegame-1')
  sleep 1
end

When(/^нажмет на кнопку "([^"]*)"$/) do |button|
  click_on "#{button}"
  sleep 1
end

То(/^попадет на страницу игры$/) do
  sleep 2
  page.assert_text('Результат')
end

# Новости
Допустим(/^видит блок с новостями$/) do
  find('div', class: 'news')
end

Если(/^он кликнет "([^"]*)" на одной из новостей$/) do |link_name|
  create(:post)

  visit('/')
  within('#post-1') do
    click_link link_name
  end
  sleep 1
end

То(/^попадет на страницу этой новости$/) do
  find('span', class: 'glyphicon-eye-open')
end

# Турнир
Допустим(/^видит блок турнира$/) do
  @tournament = create(:tournament)
  @game = create(:demogame, tournament_id: @tournament.id)
  visit('/')
  find_by_id 'tournament'
end

Если(/^он кликнет "([^"]*)"$/) do |link_name|
  click_link link_name
  sleep 10
end

То(/^попадет на страницу актуального турнира$/) do
  current_path == 'http://localhost:3000/tournaments'

end

#Проверка html_safe
When(/^есть тестовый турнир$/) do
  @tournament = create(:tournament, description: "<b>test</b>")
  create(:demogame, tournament_id: @tournament.id)
end

When(/^Пользователь заходит на сайт$/) do
  visit "/"
end

When(/^видит описание тестового турнира$/) do
  find_by_id "tournament"
end

When(/^в нем присутствует жирный текст$/) do
  @strong = find_by_id("tournament").find("b", text: "test")
end

When(/^в этом тексте отсутствуют теги "([^"]*)" и "([^"]*)"$/) do |b, strong|
  @strong.assert_no_text b
  @strong.assert_no_text strong
end

When(/^существует тематика и принадлежащая ей игра$/) do
  theme = create(:literature)
  create(:game, title: 'Тематическая Игра', theme: theme)
end

When(/^пользователь перейдет на страницу списка тематик$/) do
  visit('/themes')
end

When(/^существует несколько игр, новостей$/) do
  level = create(:level)
  theme = create(:literature)
  create(:game, title: 'Игра для поиска', theme: theme, level: level)
  create(:post, title: 'Новость, которую не найдут')
  create(:post, title: 'Новость для поиска')
end

When(/^существует несколько пользователей$/) do
  create(:user, alias: 'User to find')
  create(:user, alias: 'Just user')
end

When(/^пользователь заполнит поле для поиска словом "([^"]*)"$/) do |word|
  find('#search').set(word)
end