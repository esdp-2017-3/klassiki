# language: ru
@javascript
Функционал: Управление вопросами
  Как администратор
  Я хочу иметь возможность управлять вопросами

  Сценарий: Создание вопроса
    Допустим администратор сайта залогинился с данными:
      | email             | password |
      | admin@example.com | password |
    Если он перейдет в админ-панель
    И заполнит форму добавления вопроса данными:
      | game      | body             | answer_desc      | format | about             |
      | Test Game | Lorem Ipsum Body | Lorem Ipsum Desc | string | about lorem ipsum |
    И нажмет на кнопку для добавления картинки
    И добавит картинку:
      | attachment |
      | 30.jpg     |
    И нажмет на кнопку для добавления аудиофайла
    И добавит аудиофайл
      | attachment                   |
      | Imagine Dragons - Demons.mp3 |
    И нажмет на кнопку для добавления ответа
    И добавит ответ
      | body        |
      | test answer |
    И нажмет на кнопку для добавления вопроса
    То название "Lorem Ipsum Body" видно

  Сценарий: Изменение вопроса
    Допустим администратор сайта залогинился с данными:
      | email             | password |
      | admin@example.com | password |
    Если он перейдет в админ-панель
    И нажимает "Изменить" для вопроса "Question to test"
    И заполнит форму изменения вопроса данными:
      | game      | body             | answer_desc      | format | about       |
      | Test Game | Edit Ipsum Body  | Lorem Ipsum Desc | string | lorem ipsum |
    И нажмет на кнопку для добавления картинки
    И добавит картинку:
      | attachment |
      | 30.jpg     |
    И нажмет на кнопку для добавления аудиофайла
    И добавит аудиофайл
      | attachment                   |
      | Imagine Dragons - Demons.mp3 |
    И нажмет на кнопку для добавления ответа
    И добавит ответ
      | body            |
      | new test answer |
    И нажмет на кнопку для изменения вопроса
    То название "Edit Ipsum Body" видно

  Сценарий: Удаление вопроса
    Допустим администратор сайта залогинился с данными:
      | email             | password |
      | admin@example.com | password |
    Если он перейдет в админ-панель
    И нажимает "Удалить" для вопроса "Question to test"
    И подтверждает удаление
    То название "Question to test" не видно в списке вопросов
